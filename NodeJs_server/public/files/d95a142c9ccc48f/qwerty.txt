1.
	[AR1]
		vlan batch 5 15
		int g0/0/0
		ip addr 172.31.0.174 28
		int g0/0/0.5
		d t v 5
		a b e
		ip addr 172.31.0.206 28
		int g0/0/1
		ip addr 172.31.0.126 25
		int g0/0/1.15
		d t v 15
		a b e
		ip addr 172.31.0.129 27

	[AR2]
		vlan batch 10 15
		int g0/0/0
		ip addr 172.31.0.190 28
		int g0/0/0.10
		d t v 10
		a b e
		ip addr 172.31.0.222 28
		int g0/0/1
		ip addr 172.31.0.125 25
		int g0/0/1.15
		d t v 15
		a b e
		ip addr 172.31.0.158 27
	[AR3]
		vlan batch 5 10
		int g0/0/0
		ip addr 172.31.0.225 28
		int g0/0/1
		ip addr 172.31.0.161 28
		int g0/0/1.5
		d t v 5
		a b e
		ip addr 172.31.0.193 28
		int g0/0/2
		ip addr 172.31.0.177 28
		int g0/0/2.10
		d t v 10
		a b e
		ip addr 172.31.0.209 28
	[AR4]
		int g0/0/0
		ip addr 172.31.0.238 28
		int loo0
		ip addr 172.31.0.241 28

2. 
	[AR1]
		int g0/0/1
		vrrp vrid 1 virtual-ip 172.31.0.1
	[AR2]
		int g0/0/1
		vrrp vrid 1 virtual-ip 172.31.0.1
	3.
	[AR1]
		ospf 1 router-id 1.1.1.1
		area 0
		net 172.31.0.160 0.0.0.15
	[AR3]
		ospf 1 router-id 2.2.2.2
		area 0
		net 172.31.0.160 0.0.0.15
		area 1
		net 172.31.0.224 0.0.0.15
	[AR4]
		ospf 1 router-id 4.4.4.4
		area 1
		net 172.31.0.224 0.0.0.15
		area 2
		net 172.31.0.240 0.0.0.15
		stub
4.
	[AR1]
		rip 1
		v 2
		net 172.31.0.0
	[AR2]
		rip 1
		v 2
		net 172.31.0.0
5.
	[AR1]
		ospf 1
		import-route rip 1 type 2
		rip 1
		import-route ospf 1