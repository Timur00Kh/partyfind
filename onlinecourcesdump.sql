--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 11.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO timur;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: subscribeuser(bigint, bigint); Type: FUNCTION; Schema: public; Owner: timur
--

CREATE FUNCTION public.subscribeuser(eventid bigint, userid bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
begin
  if (select 1 from cs_event where id = eventId) = 1
     and
     (select 1 from cs_user where
         id in (select user_id from user_event
                where user_id = userId
                  and event_id in (select id from cs_event where id = eventId))) isnull
  then
    insert into user_event(user_id, event_id)
    values (userId, eventId);
  end if;
  return null;
end;
$$;


ALTER FUNCTION public.subscribeuser(eventid bigint, userid bigint) OWNER TO timur;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cs_event; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.cs_event (
    id bigint NOT NULL,
    name text,
    lessons_count integer,
    description text,
    address text,
    email text,
    phone text,
    creator_id bigint,
    begin_time timestamp without time zone,
    end_time timestamp without time zone,
    x double precision,
    y double precision,
    avatar text,
    poster text,
    CONSTRAINT count_ch CHECK ((lessons_count > 0))
);


ALTER TABLE public.cs_event OWNER TO timur;

--
-- Name: cs_user; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.cs_user (
    id bigint NOT NULL,
    first_name text,
    last_name text,
    email text,
    phone text,
    city text,
    login text NOT NULL,
    hash_password text NOT NULL,
    avatar text,
    role character varying(255),
    state character varying(255) DEFAULT 'NOT_CONFIRMED'::character varying
);


ALTER TABLE public.cs_user OWNER TO timur;

--
-- Name: email; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.email (
    user_id bigint,
    register_date timestamp without time zone,
    code text
);


ALTER TABLE public.email OWNER TO timur;

--
-- Name: event_tag; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.event_tag (
    event_id bigint,
    tag_id bigint
);


ALTER TABLE public.event_tag OWNER TO timur;

--
-- Name: file; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.file (
    id bigint NOT NULL,
    name text,
    url text,
    lesson_id bigint
);


ALTER TABLE public.file OWNER TO timur;

--
-- Name: lecturer; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.lecturer (
    id bigint NOT NULL,
    first_name text,
    last_name text,
    email text,
    phone text,
    description text,
    user_id bigint,
    avatar text
);


ALTER TABLE public.lecturer OWNER TO timur;

--
-- Name: lecturer_event; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.lecturer_event (
    lecturer_id bigint,
    event_id bigint
);


ALTER TABLE public.lecturer_event OWNER TO timur;

--
-- Name: lecturer_tag; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.lecturer_tag (
    lecturer_id bigint,
    tag_id bigint
);


ALTER TABLE public.lecturer_tag OWNER TO timur;

--
-- Name: lesson; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.lesson (
    id bigint NOT NULL,
    event_id bigint,
    index integer,
    date timestamp without time zone,
    name text,
    description text,
    avatar text,
    type text
);


ALTER TABLE public.lesson OWNER TO timur;

--
-- Name: online_courses_cs_event; Type: SEQUENCE; Schema: public; Owner: timur
--

CREATE SEQUENCE public.online_courses_cs_event
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.online_courses_cs_event OWNER TO timur;

--
-- Name: online_courses_cs_event; Type: SEQUENCE OWNED BY; Schema: public; Owner: timur
--

ALTER SEQUENCE public.online_courses_cs_event OWNED BY public.cs_event.id;


--
-- Name: online_courses_cs_user; Type: SEQUENCE; Schema: public; Owner: timur
--

CREATE SEQUENCE public.online_courses_cs_user
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.online_courses_cs_user OWNER TO timur;

--
-- Name: online_courses_cs_user; Type: SEQUENCE OWNED BY; Schema: public; Owner: timur
--

ALTER SEQUENCE public.online_courses_cs_user OWNED BY public.cs_user.id;


--
-- Name: online_courses_file; Type: SEQUENCE; Schema: public; Owner: timur
--

CREATE SEQUENCE public.online_courses_file
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.online_courses_file OWNER TO timur;

--
-- Name: online_courses_file; Type: SEQUENCE OWNED BY; Schema: public; Owner: timur
--

ALTER SEQUENCE public.online_courses_file OWNED BY public.file.id;


--
-- Name: online_courses_lecturer; Type: SEQUENCE; Schema: public; Owner: timur
--

CREATE SEQUENCE public.online_courses_lecturer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.online_courses_lecturer OWNER TO timur;

--
-- Name: online_courses_lecturer; Type: SEQUENCE OWNED BY; Schema: public; Owner: timur
--

ALTER SEQUENCE public.online_courses_lecturer OWNED BY public.lecturer.id;


--
-- Name: online_courses_lesson; Type: SEQUENCE; Schema: public; Owner: timur
--

CREATE SEQUENCE public.online_courses_lesson
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.online_courses_lesson OWNER TO timur;

--
-- Name: online_courses_lesson; Type: SEQUENCE OWNED BY; Schema: public; Owner: timur
--

ALTER SEQUENCE public.online_courses_lesson OWNED BY public.lesson.id;


--
-- Name: tag; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.tag (
    id bigint NOT NULL,
    name text
);


ALTER TABLE public.tag OWNER TO timur;

--
-- Name: online_courses_tag; Type: SEQUENCE; Schema: public; Owner: timur
--

CREATE SEQUENCE public.online_courses_tag
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.online_courses_tag OWNER TO timur;

--
-- Name: online_courses_tag; Type: SEQUENCE OWNED BY; Schema: public; Owner: timur
--

ALTER SEQUENCE public.online_courses_tag OWNED BY public.tag.id;


--
-- Name: token; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.token (
    id bigint NOT NULL,
    value text,
    user_id bigint,
    created_at timestamp without time zone,
    expired_date_time timestamp without time zone
);


ALTER TABLE public.token OWNER TO timur;

--
-- Name: online_courses_token; Type: SEQUENCE; Schema: public; Owner: timur
--

CREATE SEQUENCE public.online_courses_token
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.online_courses_token OWNER TO timur;

--
-- Name: online_courses_token; Type: SEQUENCE OWNED BY; Schema: public; Owner: timur
--

ALTER SEQUENCE public.online_courses_token OWNED BY public.token.id;


--
-- Name: user_event; Type: TABLE; Schema: public; Owner: timur
--

CREATE TABLE public.user_event (
    user_id bigint,
    event_id bigint
);


ALTER TABLE public.user_event OWNER TO timur;

--
-- Name: cs_event id; Type: DEFAULT; Schema: public; Owner: timur
--

ALTER TABLE ONLY public.cs_event ALTER COLUMN id SET DEFAULT nextval('public.online_courses_cs_event'::regclass);


--
-- Name: cs_user id; Type: DEFAULT; Schema: public; Owner: timur
--

ALTER TABLE ONLY public.cs_user ALTER COLUMN id SET DEFAULT nextval('public.online_courses_cs_user'::regclass);


--
-- Name: file id; Type: DEFAULT; Schema: public; Owner: timur
--

ALTER TABLE ONLY public.file ALTER COLUMN id SET DEFAULT nextval('public.online_courses_file'::regclass);


--
-- Name: lecturer id; Type: DEFAULT; Schema: public; Owner: timur
--

ALTER TABLE ONLY public.lecturer ALTER COLUMN id SET DEFAULT nextval('public.online_courses_lecturer'::regclass);


--
-- Name: lesson id; Type: DEFAULT; Schema: public; Owner: timur
--

ALTER TABLE ONLY public.lesson ALTER COLUMN id SET DEFAULT nextval('public.online_courses_lesson'::regclass);


--
-- Name: tag id; Type: DEFAULT; Schema: public; Owner: timur
--

ALTER TABLE ONLY public.tag ALTER COLUMN id SET DEFAULT nextval('public.online_courses_tag'::regclass);


--
-- Name: token id; Type: DEFAULT; Schema: public; Owner: timur
--

ALTER TABLE ONLY public.token ALTER COLUMN id SET DEFAULT nextval('public.online_courses_token'::regclass);


--
-- Data for Name: cs_event; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.cs_event (id, name, lessons_count, description, address, email, phone, creator_id, begin_time, end_time, x, y, avatar, poster) FROM stdin;
30	Rest2	\N	Изучаем спринг по Понедельникам. Не прогуливать!!	\N	\N	\N	4	\N	\N	7.57560014724731445	6.34345006942749023	\N	\N
31	Rest2	\N	Изучаем спринг по Понедельникам. Не прогуливать!!	\N	\N	\N	4	\N	\N	7.57560014724731445	6.34345006942749023	\N	\N
32	Rest2	\N	Изучаем спринг по Понедельникам. Не прогуливать!!	\N	\N	\N	4	\N	\N	7.57560014724731445	6.34345006942749023	\N	\N
33	Rest2	\N	Изучаем спринг по Понедельникам. Не прогуливать!!	\N	\N	\N	4	\N	\N	7.57559999999999967	6.34344999999999981	\N	\N
34	Паребрик шоу 2	\N	Изучаем паребрики по Понедельникам. Не прогуливать!! Очень интересные паребрики, нужно паребрики обязательно. Я генерирую паребрики, потому что я паребрик текста. Я паребрик.	сквер Славы, Казань	\N	\N	\N	\N	\N	55.7501949999999979	49.2069499999999991	\N	\N
35	Java-lab on English	\N	Izuchaem spring po Ponedel`nikam. Ne progulivat`!! Ochen` interesny`e pary`, nuzhno prihodit` obiazatel`no. IA generiruiu tekst, potomu chto ia generator teksta. IA molodetc.	Kremlyovskaia ulitca, 35	\N	\N	\N	\N	\N	55.7921339999999972	49.1221260000000015	\N	\N
36	Java-lab on English	\N	Izuchaem spring po Ponedel`nikam. Ne progulivat`!! Ochen` interesny`e pary`, nuzhno prihodit` obiazatel`no. IA generiruiu tekst, potomu chto ia generator teksta. IA molodetc.	Kremlyovskaia ulitca, 35	\N	\N	\N	\N	\N	55.7921339999999972	49.1221260000000015	\N	\N
37	Тест файла	\N	Изучаем спринг по Понедельникам. Не прогуливать!! Очень интересные пары, нужно приходить обязательно. Я генерирую текст, потому что я генератор текста. Я молодец.	\N	\N	\N	\N	\N	\N	55.7921333312988281	49.1221275329589844	\N	\N
8	lox	\N	java is a capital of great oop	\N	fhfh	\N	\N	\N	\N	\N	\N	\N	\N
38	Тест файла	\N	Изучаем спринг по Понедельникам. Не прогуливать!! Очень интересные пары, нужно приходить обязательно. Я генерирую текст, потому что я генератор текста. Я молодец.	\N	\N	\N	\N	\N	\N	55.7921333312988281	49.1221275329589844	\N	\N
39	Тест файла	\N	Изучаем спринг по Понедельникам. Не прогуливать!! Очень интересные пары, нужно приходить обязательно. Я генерирую текст, потому что я генератор текста. Я молодец.	\N	\N	\N	\N	\N	\N	55.7921333312988281	49.1221275329589844	\N	\N
40	Тест файла	\N	Изучаем спринг по Понедельникам. Не прогуливать!! Очень интересные пары, нужно приходить обязательно. Я генерирую текст, потому что я генератор текста. Я молодец.	\N	\N	\N	\N	\N	\N	55.7921333312988281	49.1221275329589844	\N	\N
1	java	2	java is a capital of great oop	\N	java_iava	\N	\N	2019-03-24 15:35:35.626	\N	\N	\N	\N	\N
24	Rest2	\N	Изучаем спринг по Понедельникам. Не прогуливать!!	\N	\N	\N	4	\N	\N	7.57560014724731445	6.34345006942749023	\N	\N
26	Пары Java-lab	\N	Изучаем спринг по Понедельникам. Не прогуливать!! Очень интересные пары, нужно приходить обязательно. Я генерирую текст, потому что я генератор текста. Я молодец.	\N	\N	\N	\N	\N	\N	55.7921333312988281	49.1221275329589844	\N	\N
27	Паребрик шоу	\N	Изучаем паребрики по Понедельникам. Не прогуливать!! Очень интересные паребрики, нужно паребрики обязательно. Я генерирую паребрики, потому что я паребрик текста. Я паребрик.	сквер Славы, Казань	\N	\N	\N	\N	\N	55.7501945495605469	49.20684814453125	\N	\N
51	test_file	\N	Изучаем спринг по Понедельникам. Не прогуливать!!	\N	\N	\N	3	\N	\N	7.57559999999999967	6.34344999999999981	\N	\N
57	Save	\N	Я молодец.	\N	\N	\N	\N	\N	\N	55.7921333312988281	49.1221275329589844	\N	\N
58	Save	\N	Я молодец.	\N	\N	\N	3	\N	\N	55.7921333312988281	49.1221275329589844	\N	\N
59	UpdateNew	\N	Я молодец.	\N	\N	\N	3	\N	\N	55.7921333312988281	49.1221275329589844	\N	\N
\.


--
-- Data for Name: cs_user; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.cs_user (id, first_name, last_name, email, phone, city, login, hash_password, avatar, role, state) FROM stdin;
1	User	User	User	User	User	User	User	\N	ADMIN	ACTIVE
2	Разиль	Миннеахметов	\N	\N	\N	razil0071999@gmail.com	$2a$10$quVe3/1F9hUJ/lo9XC98LOjTj8x/JDEpZnglxlUX6ITnnhaP/ekRu	\N	ADMIN	ACTIVE
4	\N	\N	\N	\N	\N	emil_p1	$2a$10$5y8O.scxKwbUfoErwBud6ONq0lKuY6Bl23TizUIgZr3.nf8HOSNVO	\N	USER	ACTIVE
3	emil	p	pashaev_emil999@mail.ru	\N	Kazan	emil_p	$2a$10$fP0Yn0JknRPlfrvQBefPkOpdz.tHC.vZt09G1LaMROF6MOt365PyS	\N	USER	ACTIVE
\.


--
-- Data for Name: email; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.email (user_id, register_date, code) FROM stdin;
1	2019-05-06 15:42:06.118646	f55095107af24820855fcbcae4a936cd
\.


--
-- Data for Name: event_tag; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.event_tag (event_id, tag_id) FROM stdin;
24	2
24	3
24	4
30	2
31	2
30	4
31	6
32	2
33	2
32	3
33	6
26	7
59	3
59	2
\.


--
-- Data for Name: file; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.file (id, name, url, lesson_id) FROM stdin;
8	File1	File1	94
9	File2	File2	94
\.


--
-- Data for Name: lecturer; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.lecturer (id, first_name, last_name, email, phone, description, user_id, avatar) FROM stdin;
3	Марсель Сидиков	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
4	Салават Забиров	\N	\N	\N	Deputy Head of JavaLab	\N	\N
2	Салават Забиров	\N	\N	\N	Deputy Head of JavaLab	\N	\N
36	Паребрик Мэн	\N	\N	\N	Head of Паребрик, Казан	\N	https://pp.userapi.com/c851024/v851024767/fd7de/wO6I8KoW9P4.jpg
34	Марсель Сидиков	\N	\N	\N	Head of Java-lab, ITIS	\N	https://pp.userapi.com/c847218/v847218793/1daafa/GSMFIR8Md8c.jpg
37	Паребрик Мэн	\N	\N	\N	Head of Паребрик, Казан	\N	https://pp.userapi.com/c851024/v851024767/fd7de/wO6I8KoW9P4.jpg
38	Паребрик Мэн	\N	\N	\N	Head of Паребрик, Казан	\N	https://pp.userapi.com/c851024/v851024767/fd7de/wO6I8KoW9P4.jpg
39	Марсель Сидиков	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
40	Салават Забиров	\N	\N	\N	Deputy Head of JavaLab	\N	\N
41	Марсель Сидиков	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
42	Салават Забиров	\N	\N	\N	Deputy Head of JavaLab	\N	\N
43	Марсель Сидиков	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
44	Салават Забиров	\N	\N	\N	Deputy Head of JavaLab	\N	\N
45	Марсель Сидиков	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
46	Салават Забиров	\N	\N	\N	Deputy Head of JavaLab	\N	\N
47	Марсель Сидиков	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
48	Паребрик Мэн	\N	\N	\N	Head of Паребрик, Казан	\N	https://pp.userapi.com/c851024/v851024767/fd7de/wO6I8KoW9P4.jpg
35	Cалават Забиров	\N	\N	\N	Deputy Head of JavaLab	\N	https://pp.userapi.com/c851024/v851024078/f06fb/zOYpSl3_jvs.jpg
32	Марсель Cидиков	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
33	Cалават Забиров	\N	\N	\N	Deputy Head of JavaLab	\N	\N
49	Marsel	Sidicov	\N	\N	Head of Java-lab, ITIS	\N	https://pp.userapi.com/c847218/v847218793/1daafa/GSMFIR8Md8c.jpg
50	Salavat	Zabirov	\N	\N	Deputy Head of JavaLab	\N	https://pp.userapi.com/c851024/v851024078/f06fb/zOYpSl3_jvs.jpg
51	Ghost	First	\N	\N	Ghost of JavaLab	\N	https://pp.userapi.com/c851024/v851024078/f06fb/zOYpSl3_jvs.jpg
52	Ghost	Second	\N	\N	Ghost of JavaLab	\N	https://pp.userapi.com/c851024/v851024078/f06fb/zOYpSl3_jvs.jpg
53	Марсель Сидиков	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
54	Marsel	Sidicov	\N	\N	Head of Java-lab, ITIS	\N	https://pp.userapi.com/c847218/v847218793/1daafa/GSMFIR8Md8c.jpg
55	Salavat	Zabirov	\N	\N	Deputy Head of JavaLab	\N	https://pp.userapi.com/c851024/v851024078/f06fb/zOYpSl3_jvs.jpg
56	Ghost	First	\N	\N	Ghost of JavaLab	\N	https://pp.userapi.com/c851024/v851024078/f06fb/zOYpSl3_jvs.jpg
57	Ghost	Second	\N	\N	Ghost of JavaLab	\N	https://pp.userapi.com/c851024/v851024078/f06fb/zOYpSl3_jvs.jpg
58	Марсель Cидиков +100500	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
59	Марсель Cидиков +100500	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
60	Марсель Cидиков +100500	\N	\N	\N	Head of Java-lab, ITIS	\N	\N
\.


--
-- Data for Name: lecturer_event; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.lecturer_event (lecturer_id, event_id) FROM stdin;
32	24
36	27
39	30
40	30
41	31
42	31
43	32
44	32
46	33
48	34
49	35
50	35
51	35
52	35
54	36
55	36
56	36
57	36
35	59
35	59
35	59
35	59
35	59
35	58
35	57
35	26
35	59
34	59
34	59
34	59
34	59
34	59
34	58
34	57
34	26
34	59
\.


--
-- Data for Name: lecturer_tag; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.lecturer_tag (lecturer_id, tag_id) FROM stdin;
32	2
32	3
34	2
35	2
\.


--
-- Data for Name: lesson; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.lesson (id, event_id, index, date, name, description, avatar, type) FROM stdin;
2	1	1	\N	trigger	trigger is a trigger	\N	\N
3	1	2	\N	java	java tea	\N	\N
100	57	\N	2019-04-29 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
101	57	\N	2019-04-08 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
102	57	\N	2019-04-15 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
103	57	\N	2019-04-22 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
104	57	\N	2019-04-08 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
105	57	\N	2019-04-15 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
106	57	\N	2019-04-22 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
107	57	\N	2019-04-29 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
33	27	\N	2019-04-08 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	https://pp.userapi.com/c851024/v851024078/f06fb/zOYpSl3_jvs.jpg	\N
34	27	\N	2019-04-08 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
35	27	\N	2019-04-15 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
36	27	\N	2019-04-15 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
37	27	\N	2019-04-22 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
38	27	\N	2019-04-22 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
39	27	\N	2019-04-29 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
40	27	\N	2019-04-29 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
26	26	\N	2019-04-08 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	https://pp.userapi.com/c847218/v847218793/1daafa/GSMFIR8Md8c.jpg	\N
108	58	\N	2019-04-29 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
57	30	\N	2019-03-25 14:09:26.562	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
58	31	\N	2019-03-25 14:09:26.562	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
59	32	\N	2019-03-25 14:09:26.562	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
109	58	\N	2019-04-08 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
61	34	\N	2019-04-08 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
62	34	\N	2019-04-08 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
63	34	\N	2019-04-15 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
64	34	\N	2019-04-15 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
65	34	\N	2019-04-22 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
66	34	\N	2019-04-22 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
67	34	\N	2019-04-29 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
68	34	\N	2019-04-29 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
69	35	\N	2019-04-08 14:00:00	DataBases lesson	Izuchim trigery`. Budet zbs para, prihodite	\N	\N
70	35	\N	2019-04-08 15:40:00	Java lesson	Izuchim spring. Budet zbs para, prihodite	\N	\N
71	35	\N	2019-04-15 14:00:00	DataBases lesson	Izuchim trigery`. Budet zbs para, prihodite	\N	\N
72	35	\N	2019-04-15 15:40:00	Java lesson	Izuchim spring. Budet zbs para, prihodite	\N	\N
73	35	\N	2019-04-22 14:00:00	DataBases lesson	Izuchim trigery`. Budet zbs para, prihodite	\N	\N
74	35	\N	2019-04-22 15:40:00	Java lesson	Izuchim spring. Budet zbs para, prihodite	\N	\N
75	35	\N	2019-04-29 14:00:00	DataBases lesson	Izuchim trigery`. Budet zbs para, prihodite	\N	\N
76	35	\N	2019-04-29 15:40:00	Java lesson	Izuchim spring. Budet zbs para, prihodite	\N	\N
77	36	\N	2019-04-08 14:00:00	DataBases lesson	Izuchim trigery`. Budet zbs para, prihodite	\N	\N
78	36	\N	2019-04-08 15:40:00	Java lesson	Izuchim spring. Budet zbs para, prihodite	\N	\N
79	36	\N	2019-04-15 14:00:00	DataBases lesson	Izuchim trigery`. Budet zbs para, prihodite	\N	\N
80	36	\N	2019-04-15 15:40:00	Java lesson	Izuchim spring. Budet zbs para, prihodite	\N	\N
81	36	\N	2019-04-22 14:00:00	DataBases lesson	Izuchim trigery`. Budet zbs para, prihodite	\N	\N
82	36	\N	2019-04-22 15:40:00	Java lesson	Izuchim spring. Budet zbs para, prihodite	\N	\N
83	36	\N	2019-04-29 14:00:00	DataBases lesson	Izuchim trigery`. Budet zbs para, prihodite	\N	\N
84	36	\N	2019-04-29 15:40:00	Java lesson	Izuchim spring. Budet zbs para, prihodite	\N	\N
110	58	\N	2019-04-15 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
111	58	\N	2019-04-22 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
112	58	\N	2019-04-08 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
113	58	\N	2019-04-15 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
114	58	\N	2019-04-22 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
115	58	\N	2019-04-29 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
116	59	\N	2019-04-29 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
117	59	\N	2019-04-08 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
118	59	\N	2019-04-15 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
94	51	\N	2019-03-25 14:09:26.562	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
119	59	\N	2019-04-22 15:40:00	Пара по Java	Изучим спринг. Будет збс пара, приходите	\N	\N
120	59	\N	2019-04-08 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
121	59	\N	2019-04-15 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
122	59	\N	2019-04-22 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
123	59	\N	2019-04-29 14:00:00	Пара по БД	Изучим тригеры. Будет збс пара, приходите	\N	\N
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- COPY public.spatial_ref_sys  FROM stdin;
-- \.


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.tag (id, name) FROM stdin;
3	tag3
2	tag2
4	tag4
5	tag5
6	tag6
7	Java
8	PareBrick
\.


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: timur
--

COPY public.token (id, value, user_id, created_at, expired_date_time) FROM stdin;
13	1SkzadaWWG	3	2019-05-19 20:24:01.390406	2019-05-20 20:24:01.390406
\.


--
-- Data for Name: us_gaz; Type: TABLE DATA; Schema: public; Owner: postgres
--

-- COPY public.us_gaz  FROM stdin;
