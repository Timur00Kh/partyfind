package com.onlinecourses.schedulers;

import com.onlinecourses.repositories.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Configuration
@EnableScheduling
public class TokenScheduler {
    @Autowired
    private TokenRepository tokenRepository;

    @Scheduled(cron = "0 0 0 * * ?")
    @Transactional
    public void removeExpiredTokens() {
        tokenRepository.deleteTokensByExpiredDateTimeBefore(LocalDateTime.now());
    }
}