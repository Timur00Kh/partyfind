package com.onlinecourses.transfer;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.models.Event;
import com.onlinecourses.models.Point;
import com.onlinecourses.models.View;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExtendedEventDto {
    @JsonView(value = {View.Event.class})
    private Long id;

    @JsonView(value = {View.Event.class})
    private String name;

    @JsonView(value = {View.Event.class})
    private String description;

    @JsonView(value = {View.Event.class})
    private String avatar;

    @JsonView(value = {View.ExtendedEvent.class})
    private String poster;

    @JsonView(value = View.ExtendedEvent.class)
    private String email;

    @JsonView(value = View.ExtendedEvent.class)
    private String phone;

    @JsonView(value = View.SearchEvent.class)
    private String address;

    @JsonView(value = View.SearchEvent.class)
    private Point point;

    @JsonView(value = View.ExtendedEvent.class)
    private LocalDateTime beginTime;

    @JsonView(value = View.ExtendedEvent.class)
    private LocalDateTime endTime;

    @JsonView(value = {View.Event.class})
    private Integer lessonsCount;

    @JsonView(value = {View.SearchEvent.class})
    private List<LessonDto> lessons;

    @JsonView(value = {View.SearchEvent.class})
    private List<LecturerDtoForEvent> lecturers;

    @JsonView(value = View.ExtendedEvent.class)
    private List<UserDto> users;

    @JsonView(value = View.ExtendedEvent.class)
    private Boolean subscriber;

    @JsonView(value = View.ExtendedEvent.class)
    private List<TagDto> tags;

    public static ExtendedEventDto from(Event event) {
        return ExtendedEventDto.builder()
                .id(event.getId())
                .name(event.getName())
                .description(event.getDescription())
                .avatar(event.getAvatar())
                .poster(event.getPoster())
                .point(event.getPoint())
                .email(event.getEmail())
                .phone(event.getPhone())
                .address(event.getAddress())
                .beginTime(event.getBeginTime())
                .endTime(event.getEndTime())
                .lessonsCount(event.getLessonsCount())
                .users(UserDto.from(event.getUsers()))
                .lessons(LessonDto.from(event.getLessons()))
                .point(event.getPoint())
                .subscriber(false)
                .tags(TagDto.from(event.getTags()))
                .lecturers(LecturerDtoForEvent.from(event.getLecturers()))
                .build();
    }

    public static List<ExtendedEventDto> from(List<Event> events) {
        if (events != null) {
            return events.stream().map(ExtendedEventDto::from).collect(Collectors.toList());
        } else return null;
    }
}
