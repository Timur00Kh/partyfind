package com.onlinecourses.transfer;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.models.User;
import com.onlinecourses.models.View;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = {"events", "createdEvents"})
public class ExtendedUserDto {
    @JsonView(value = {View.User.class})
    private Long id;

    @JsonView(value = {View.User.class})
    private String firstName;

    @JsonView(value = {View.User.class})
    private String lastName;

    @JsonView(value = {View.User.class})
    private String login;

    @JsonView(value = {View.ExtendedUser.class})
    private String email;

    @JsonView(value = {View.ExtendedUser.class})
    private String city;

    @JsonView(value = {View.ExtendedUser.class})
    private String phone;

    @JsonView(value = {View.User.class})
    private String avatar;

    @JsonView(value = {View.ExtendedUser.class})
    private List<EventDto> events;

    @JsonView(value = {View.ExtendedUser.class})
    private List<EventDto> createdEvents;

    public  static ExtendedUserDto from(User user) {
        return ExtendedUserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .login(user.getLogin())
                .email(user.getEmail())
                .city(user.getCity())
                .phone(user.getPhone())
                .avatar(user.getAvatar())
                .events(EventDto.from(user.getEvents()))
                .createdEvents(EventDto.from(user.getCreatedEvents()))
                .build();
    }

    public static List<ExtendedUserDto> from(List<User> users) {
        if (users != null) {
            return users.stream().map(ExtendedUserDto::from).collect(Collectors.toList());
        } else return null;
    }
}
