package com.onlinecourses.transfer;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.models.Lecturer;
import com.onlinecourses.models.View;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString(exclude = {"tags", "events"})
public class ExtendedLecturerDto {
    @JsonView(value = {View.Lecturer.class,})
    private Long id;

    @JsonView(value = {View.Lecturer.class})
    private String firstName;

    @JsonView(value = {View.Lecturer.class})
    private String lastName;

    @JsonView(value = {View.ExtendedLecturer.class})
    private String email;

    @JsonView(value = {View.ExtendedLecturer.class})
    private String phone;

    @JsonView(value = {View.Lecturer.class})
    private String description;

    @JsonView(value = {View.Lecturer.class})
    private String avatar;

    @JsonView(value = {View.SearchLecturer.class})
    private List<TagDto> tags;

    @JsonView(value = {View.ExtendedLecturer.class})
    List<EventDto> events;

    public static ExtendedLecturerDto from(Lecturer lecturer) {
        return ExtendedLecturerDto.builder()
                .id(lecturer.getId())
                .firstName(lecturer.getFirstName())
                .lastName(lecturer.getLastName())
                .email(lecturer.getEmail())
                .phone(lecturer.getPhone())
                .description(lecturer.getDescription())
                .avatar(lecturer.getAvatar())
                .events(EventDto.from(lecturer.getEvents()))
                .tags(TagDto.from(lecturer.getTags()))
                .build();
    }

    public static List<ExtendedLecturerDto> from(List<Lecturer> lecturers) {
        if (lecturers != null) {
            return lecturers.stream().map(ExtendedLecturerDto::from).collect(Collectors.toList());
        } else return null;
    }
}