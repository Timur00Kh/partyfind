package com.onlinecourses.transfer;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.models.Tag;
import com.onlinecourses.models.View;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonView(value = {View.Tag.class})
public class TagDto {
    private Long id;

    private String name;

    public static TagDto from(Tag tag) {
        return TagDto.builder()
                .id(tag.getId())
                .name(tag.getName())
                .build();
    }

    public static List<TagDto> from(List<Tag> tags) {
        if (tags != null) {
            return tags.stream().map(TagDto::from).collect(Collectors.toList());
        } else return null;
    }
}
