package com.onlinecourses.transfer;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.models.User;
import com.onlinecourses.models.View;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonView(value = {View.User.class})
public class UserDto {
    private Long id;

    private String login;
    private String firstName;
    private String lastName;
    private String avatar;

    public static UserDto from(User user) {
        return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .login(user.getLogin())
                .avatar(user.getAvatar())
                .build();
    }

    public static List<UserDto> from(List<User> users) {
        if (users != null) {
            return users.stream().map(UserDto::from).collect(Collectors.toList());
        } else return null;
    }
}
