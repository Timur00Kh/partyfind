package com.onlinecourses.transfer;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.models.Lecturer;
import com.onlinecourses.models.View;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LecturerDtoForEvent {
    @JsonView(value = {View.Lecturer.class})
    private Long id;

    @JsonView(value = {View.Lecturer.class})
    private String firstName;

    @JsonView(value = {View.Lecturer.class})
    private String lastName;

    @JsonView(value = {View.Lecturer.class})
    private String description;

    @JsonView(value = {View.Lecturer.class})
    private String avatar;

    public static LecturerDtoForEvent from(Lecturer lecturer) {
        return LecturerDtoForEvent.builder()
                .id(lecturer.getId())
                .firstName(lecturer.getFirstName())
                .lastName(lecturer.getLastName())
                .description(lecturer.getDescription())
                .avatar(lecturer.getAvatar())
                .build();
    }

    public static List<LecturerDtoForEvent> from(List<Lecturer> lecturers) {
        if (lecturers != null) {
            return lecturers.stream().map(LecturerDtoForEvent::from).collect(Collectors.toList());
        } else return null;
    }
}
