package com.onlinecourses.transfer;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.models.Event;
import com.onlinecourses.models.Point;
import com.onlinecourses.models.View;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EventDto {
    @JsonView(value = {View.Event.class})
    private Long id;

    @JsonView(value = {View.Event.class})
    private String name;

    @JsonView(value = {View.Event.class})
    private String description;

    @JsonView(value = {View.Event.class})
    private String avatar;

    @JsonView(value = {View.SearchEvent.class})
    private Point point;

    @JsonView(value = {View.SearchEvent.class})
    private List<LessonDto> lessons;

    @JsonView(value = {View.SearchEvent.class})
    private List<LecturerDtoForEvent> lecturers;

    public static EventDto from(Event event) {
        return EventDto.builder()
                .id(event.getId())
                .name(event.getName())
                .description(event.getDescription())
                .avatar(event.getAvatar())
                .lessons(LessonDto.from(event.getLessons()))
                .point(event.getPoint())
                .lecturers(LecturerDtoForEvent.from(event.getLecturers()))
                .build();
    }

    public static List<EventDto> from(List<Event> events) {
        if (events != null) {
            return events.stream().map(EventDto::from).collect(Collectors.toList());
        } else return null;
    }
}