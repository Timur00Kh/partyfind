package com.onlinecourses.transfer;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.models.FileModel;
import com.onlinecourses.models.Lesson;
import com.onlinecourses.models.View;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonDto {
    @JsonView(value = {View.Lesson.class})
    private Long id;

    @JsonView(value = {View.Lesson.class})
    private String name;

    @JsonView(value = {View.Lesson.class})
    private String description;

    @JsonView(value = {View.Lesson.class})
    private String avatar;

    @JsonView(value = {View.Lesson.class})
    private LocalDateTime date;

    @JsonView(value = {View.Lesson.class})
    private List<FileModel> files;

    public static LessonDto from(Lesson lesson) {
        return LessonDto.builder()
                .id(lesson.getId())
                .name(lesson.getName())
                .description(lesson.getDescription())
                .date(lesson.getDate())
                .avatar(lesson.getAvatar())
                .files(lesson.getFiles())
                .build();
    }

    public static List<LessonDto> from(List<Lesson> lessons) {
        if (lessons != null) {
            return lessons.stream().map(LessonDto::from).collect(Collectors.toList());
        } else return null;
    }
}
