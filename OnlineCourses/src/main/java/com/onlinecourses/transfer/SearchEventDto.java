package com.onlinecourses.transfer;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.models.Event;
import com.onlinecourses.models.Point;
import com.onlinecourses.models.Tag;
import com.onlinecourses.models.View;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SearchEventDto {
    @JsonView(value = {View.Event.class})
    private Long id;

    @JsonView(value = {View.Event.class})
    private String name;

    @JsonView(value = {View.Event.class})
    private String description;

    @JsonView(value = {View.Event.class})
    private Point point;

    @JsonView(value = {View.Event.class})
    private String avatar;

    @JsonView(value = {View.SearchEvent.class})
    private List<Tag> tags;

    @JsonView(value = {View.SearchEvent.class})
    private List<LecturerDto> lecturers;

    public static SearchEventDto from(Event event) {
        return SearchEventDto.builder()
                .id(event.getId())
                .name(event.getName())
                .description(event.getDescription())
                .point(event.getPoint())
                .lecturers(LecturerDto.from(event.getLecturers()))
                .tags(event.getTags())
                .avatar(event.getAvatar())
                .build();
    }

    public static List<SearchEventDto> from(List<Event> events) {
        if (events != null) {
            return events.stream().map(SearchEventDto::from).collect(Collectors.toList());
        } else return null;
    }
}
