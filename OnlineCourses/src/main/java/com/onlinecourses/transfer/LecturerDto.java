package com.onlinecourses.transfer;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.models.Event;

import com.onlinecourses.models.Lecturer;
import com.onlinecourses.models.View;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LecturerDto {
    @JsonView(value = View.Lecturer.class)
    private Long id;

    @JsonView(value = View.Lecturer.class)
    private String firstName;

    @JsonView(value = View.Lecturer.class)
    private String lastName;

    @JsonView(value = View.Lecturer.class)
    private String description;

    @JsonView(value = View.Lecturer.class)
    private String avatar;

    @JsonView(value = View.SearchLecturer.class)
    private List<Event> events;

    public static LecturerDto from(Lecturer lecturer) {
        return LecturerDto.builder()
                .id(lecturer.getId())
                .firstName(lecturer.getFirstName())
                .lastName(lecturer.getLastName())
                .description(lecturer.getDescription())
                .events(lecturer.getEvents())
                .avatar(lecturer.getAvatar())
                .build();
    }

    public static List<LecturerDto> from(List<Lecturer> lecturers) {
        if (lecturers != null) {
            return lecturers.stream().map(LecturerDto::from).collect(Collectors.toList());
        } else return null;
    }
}