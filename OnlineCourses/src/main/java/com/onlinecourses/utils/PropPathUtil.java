package com.onlinecourses.utils;

import lombok.Getter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.net.URLDecoder;


@Service
public class PropPathUtil {
    
    @Autowired
    private Environment environment;

    @Getter
    private String decodedPath;

    @SneakyThrows
    PropPathUtil(Environment environment){
        this.environment = environment;
        String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        decodedPath =  URLDecoder.decode(path, "UTF-8").split("/OnlineCourses/target/classes")[0];
    }
    
    public String getProperty(String key){
        return environment.getProperty(key);
    }

}
