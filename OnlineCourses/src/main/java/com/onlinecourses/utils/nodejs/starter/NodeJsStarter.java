package com.onlinecourses.utils.nodejs.starter;

import com.onlinecourses.utils.PropPathUtil;
import com.onlinecourses.utils.nodejs.threads.NodeJSThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NodeJsStarter {

    @Autowired
    NodeJsStarter(PropPathUtil propPathUtil){
        Thread thread = new NodeJSThread(propPathUtil);
        thread.start();
    }
}
