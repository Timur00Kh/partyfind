package com.onlinecourses.utils.nodejs.threads;


import com.onlinecourses.utils.PropPathUtil;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;

public class NodeJSThread extends Thread {

    private PropPathUtil propPathUtil;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public NodeJSThread(PropPathUtil propPathUtil){
        this.propPathUtil =  propPathUtil;
    }

    @Override
    @SneakyThrows
    public void run() {
        String path = propPathUtil.getDecodedPath() + propPathUtil.getProperty("nodejs.index.js.path");
        scriptGenerator(propPathUtil.getProperty("nodejs.port"), new File(path));
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec(propPathUtil.getProperty("nodejs.command"), null, new File(path.split("index.js")[0]));
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String c;
        while ((c = stdInput.readLine()) != null)
            logger.info(c);
    }

    @SneakyThrows
    private void scriptGenerator(String port, File index) {
        String script = "var express = require('express');\n" +
                "var multer = require('multer');\n" +
                "var fs = require('fs');\n" +
                "var app = express();\n" +
                "app.use(express.static('public'));\n" +
                "app.post('/upload', function (req, res, next) {\n" +
                "    var headers = req.headers;\n" +
                "    var storage = multer.diskStorage({\n" +
                "        destination: function (req, file, callback) {\n" +
                "            var dir = './public/files/' + headers.dir + '/';\n" +
                "            if (!fs.existsSync(dir))\n" +
                "                fs.mkdirSync(dir);\n" +
                "            callback(null, dir);\n" +
                "        },\n" +
                "        filename: function (req, file, callback) {\n" +
                "            callback(null, file.originalname);\n" +
                "        }\n" +
                "    });\n" +
                "    var upload = multer({storage: storage}).array('files');\n" +
                "    upload(req, res, next);\n" +
                "    res.end();\n" +
                "});\n" +
                "app.post('/uploadPhoto', function (req, res, next) {\n" +
                "    var headers = req.headers;\n" +
                "    var storage = multer.diskStorage({\n" +
                "        destination: function (req, file, callback) {\n" +
                "            var dir = './public/' + headers.dir + '/';\n" +
                "            if (!fs.existsSync(dir))\n" +
                "                fs.mkdirSync(dir);\n" +
                "            callback(null, dir);\n" +
                "        },\n" +
                "        filename: function (req, file, callback) {\n" +
                "            callback(null, file.originalname);\n" +
                "        }\n" +
                "    });\n" +
                "    var upload = multer({storage: storage}).array('files');\n" +
                "    upload(req, res, next);\n" +
                "    res.end();\n" +
                "});\n" +
                "console.log(\"Static NodeJS server started at "+ port + "\");\n" +
                "app.listen(" + port + ");";
        PrintWriter writer = new PrintWriter(index, "UTF-8");
        writer.print(script);
        writer.close();
    }
}
