package com.onlinecourses.models;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString(exclude = {"tags", "events"})
@Entity
@Table(name = "lecturer")
public class Lecturer {
    @JsonView(value = {View.Lecturer.class,})
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonView(value = {View.Lecturer.class})
    @Column(name = "first_name")
    private String firstName;

    @JsonView(value = {View.Lecturer.class})
    @Column(name = "last_name")
    private String lastName;

    @JsonView(value = {View.ExtendedLecturer.class})
    private String email;

    @JsonView(value = {View.ExtendedLecturer.class})
    private String phone;

    @JsonView(value = {View.Lecturer.class})
    private String description;

    @JsonView(value = {View.Lecturer.class})
    private String avatar;

    @JsonView(value = {View.SearchLecturer.class})
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "lecturer_tag",
            joinColumns = @JoinColumn(name = "lecturer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags;

    @JsonView(value = {View.ExtendedLecturer.class})
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "lecturer_event",
            joinColumns = @JoinColumn(name = "lecturer_id"),
            inverseJoinColumns = @JoinColumn(name = "event_id"))
    List<Event> events;
}