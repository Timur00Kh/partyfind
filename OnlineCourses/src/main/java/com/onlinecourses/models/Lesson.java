package com.onlinecourses.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString(exclude = {"event", "files"})
@Entity
@Table(name = "lesson")
public class Lesson {
    @JsonView(value = View.Lesson.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonView(value = View.Lesson.class)
    private String name;

    @JsonView(value = View.Lesson.class)
    private String description;

    @JsonView(value = View.Lesson.class)
    private String type;

    @JsonView(value = View.Lesson.class)
    private String avatar;

    @JsonView(value = View.Lesson.class)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lesson")
    private List<FileModel> files;

    @JsonView(value = View.Lesson.class)
    private LocalDateTime date;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;

    @JsonView(value = View.Lesson.class)
    private Integer index;
}