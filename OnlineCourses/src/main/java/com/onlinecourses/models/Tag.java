package com.onlinecourses.models;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.util.*;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString(exclude = {"lecturers", "events"})
@Entity
@Table(name = "tag")
public class Tag {
    @JsonView(value = View.Tag.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonView(value = View.Tag.class)
    private String name;

    @ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
    private List<Event> events;

    @ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
    private List<Lecturer> lecturers;
}