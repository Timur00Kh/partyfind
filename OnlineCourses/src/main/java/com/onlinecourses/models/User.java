package com.onlinecourses.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = {"events", "tokens", "createdEvents"})
@Entity
@Table(name = "cs_user")
public class User {
    @JsonView(value = {View.User.class})
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonView(value = {View.User.class})
    @Column(name = "first_name")
    private String firstName;

    @JsonView(value = {View.User.class})
    @Column(name = "last_name")
    private String lastName;

    @JsonView(value = {View.User.class})
    private String login;

    @JsonView(value = {View.ExtendedUser.class})
    private String email;

    @JsonView(value = {View.ExtendedUser.class})
    private String city;

    @JsonView(value = {View.ExtendedUser.class})
    private String phone;

    @JsonView(value = {View.User.class})
    private String avatar;

    @Column(name = "hash_password")
    private String hashPassword;

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @OneToMany(mappedBy = "user")
    private List<Token> tokens;

    @JsonView(value = {View.ExtendedUser.class})
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_event",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "event_id"))
    private List<Event> events;

    @JsonView(value = {View.ExtendedUser.class})
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "creator")
    private List<Event> createdEvents;
}