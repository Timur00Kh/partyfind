package com.onlinecourses.models;

public enum Role {
    ANONYMOUS, USER, ADMIN
}