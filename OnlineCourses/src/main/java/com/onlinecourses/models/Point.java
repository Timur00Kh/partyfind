package com.onlinecourses.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Embeddable
public class Point {

    @JsonView(value = View.SearchEvent.class)
    private Double x;

    @JsonView(value = View.SearchEvent.class)
    private Double y;
}
