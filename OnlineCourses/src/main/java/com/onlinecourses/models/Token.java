package com.onlinecourses.models;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonView(value = View.ExtendedUser.class)
@Entity
@ToString(exclude = "user")
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonView(value = View.ExtendedUser.class)
    private String value;

    private LocalDateTime createdAt;
    private LocalDateTime expiredDateTime;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public boolean isNotExpired() {
        return LocalDateTime.now().isBefore(expiredDateTime);
    }
}
