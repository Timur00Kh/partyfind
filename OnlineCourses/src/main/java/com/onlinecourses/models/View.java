package com.onlinecourses.models;

public final class View {
    public interface Event{}

    public interface SearchEvent extends Event, Lecturer, Tag{}

    public interface ExtendedEvent extends SearchEvent, User, Lesson{}

    public interface User{}

    public interface ExtendedUser extends User, Event, Lecturer{}

    public interface Lecturer{}

    public interface SearchLecturer extends Lecturer, Event, Tag{}

    public interface ExtendedLecturer extends SearchLecturer{}

    public interface Lesson{}

    public interface Tag{}
}
