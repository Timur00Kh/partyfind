package com.onlinecourses.models;

public enum State {
    BANNED, DELETED, ACTIVE;
}
