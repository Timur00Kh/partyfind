package com.onlinecourses.models;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString(exclude = {"creator", "lecturers", "tags", "users", "lessons"})
@Entity
@Table(name = "cs_event")
public class Event {
    @JsonView(value = {View.Event.class})
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonView(value = {View.Event.class})
    private String name;

    @JsonView(value = {View.Event.class})
    private String description;

    @JsonView(value = {View.Event.class})
    private String avatar;

    @JsonView(value = {View.ExtendedEvent.class})
    private String poster;

    @JsonView(value = View.ExtendedEvent.class)
    private String email;

    @JsonView(value = View.ExtendedEvent.class)
    private String phone;

    @JsonView(value = View.SearchEvent.class)
    private String address;

    @JsonView(value = View.SearchEvent.class)
    @Embedded
    private Point point;

    @JsonView(value = View.ExtendedEvent.class)
    @Column(name = "begin_time")
    private LocalDateTime beginTime;

    @JsonView(value = View.ExtendedEvent.class)
    @Column(name = "end_time")
    private LocalDateTime endTime;

    @JsonView(value = {View.Event.class})
    @Column(name = "lessons_count")
    private Integer lessonsCount;

    @JsonView(value = {View.ExtendedEvent.class})
    @ManyToOne()
    @JoinColumn(name = "creator_id")
    private User creator;

    @JsonView(value = {View.ExtendedEvent.class})
    @ManyToMany(mappedBy = "events", fetch = FetchType.LAZY)
    @OrderBy("first_name, last_name ASC")
    private List<Lecturer> lecturers;

    @JsonView(value = {View.SearchEvent.class})
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "event_tag",
            joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags;

    @JsonView(value = {View.ExtendedEvent.class})
    @OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
    @OrderBy("index ASC")
    private List<Lesson> lessons;

    @JsonView(value = View.ExtendedEvent.class)
    @ManyToMany(mappedBy = "events", fetch = FetchType.LAZY)
    private List<User> users;
}