package com.onlinecourses.forms;

import com.onlinecourses.models.Lesson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonForm {
    private Long id;

    private String name;
    private String description;
    private String type;

    private Long eventId;

    private LocalDateTime date;

    private List<FileForm> files;

    public static Lesson convert(LessonForm lessonForm) {
        return Lesson.builder()
                .id(lessonForm.getId())
                .name(lessonForm.getName())
                .description(lessonForm.getDescription())
                .date(lessonForm.getDate())
                .type(lessonForm.getType())
                .files(FileForm.convert(lessonForm.getFiles()))
                .build();
    }

    public static List<Lesson> convert(List<LessonForm> lessonForms) {
        return lessonForms.stream().map(LessonForm::convert).collect(Collectors.toList());
    }
}