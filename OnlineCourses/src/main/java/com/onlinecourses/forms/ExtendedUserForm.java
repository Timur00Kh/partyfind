package com.onlinecourses.forms;

import com.onlinecourses.models.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExtendedUserForm {
    private String firstName;
    private String lastName;
    private String city;
    private String phone;
    private String avatar;

    private String email;
    private String password;

    public static User convert(ExtendedUserForm userForm, User user) {
        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        user.setEmail(userForm.getEmail());
        user.setCity(userForm.getCity());
        user.setPhone(userForm.getPhone());
        user.setAvatar(userForm.getAvatar());
        return user;
    }
}