package com.onlinecourses.forms;

import com.onlinecourses.models.FileModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileForm {
    private Long id;

    private String name;
    private String url;

    public static FileModel convert(FileForm fileForm) {
        return FileModel.builder()
                .id(fileForm.getId())
                .name(fileForm.getName())
                .url(fileForm.getUrl())
                .build();
    }

    public static List<FileModel> convert(List<FileForm> fileForms) {
        return fileForms.stream().map(FileForm::convert).collect(Collectors.toList());
    }
}
