package com.onlinecourses.forms;

import com.onlinecourses.models.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class EventForm {
    private String name;
    private String description;
    private String address;
    private String email;
    private String phone;

    private Integer lessonsCount;

    private LocalDateTime beginTime;
    private LocalDateTime endTime;

    private Point point;

    private List<LessonForm> lessons;
    private List<LecturerForm> lecturers;

    public static Event convert(EventForm eventForm) {
        return Event.builder()
                .name(eventForm.getName())
                .address(eventForm.getAddress())
                .description(eventForm.getDescription())
                .beginTime(eventForm.getBeginTime())
                .endTime(eventForm.getEndTime())
                .point(eventForm.getPoint())
                .build();
    }
}