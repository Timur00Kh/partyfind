package com.onlinecourses.forms;

import com.onlinecourses.models.Lecturer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LecturerForm {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String description;

    public static Lecturer convert(LecturerForm form) {
        return Lecturer.builder()
                .id(form.getId())
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .description(form.getDescription())
                .email(form.getEmail())
                .phone(form.getPhone())
                .build();
    }

    public static List<Lecturer> convert(List<LecturerForm> lecturerForms) {
        return lecturerForms.stream().map(LecturerForm::convert).collect(Collectors.toList());
    }
}
