package com.onlinecourses.forms;

import com.onlinecourses.models.Lecturer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExtendedLecturerForm {
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String description;
    private String avatar;

    private List<EventFormForCollections> events;
    private List<TagForm> tags;

    public static Lecturer convert(ExtendedLecturerForm form, Lecturer lecturer) {
        lecturer.setFirstName(form.getFirstName());
        lecturer.setLastName(form.getLastName());
        lecturer.setEmail(form.getEmail());
        lecturer.setDescription(form.getDescription());
        lecturer.setPhone(form.getPhone());
        lecturer.setAvatar(form.getAvatar());
        return lecturer;
    }
}
