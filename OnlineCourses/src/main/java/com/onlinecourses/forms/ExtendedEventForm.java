package com.onlinecourses.forms;

import com.onlinecourses.models.Event;
import com.onlinecourses.models.Point;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class ExtendedEventForm {
    private String name;
    private String description;
    private String address;
    private String email;
    private String phone;

    private Integer lessonsCount;

    private LocalDateTime beginTime;
    private LocalDateTime endTime;

    private Point point;

    private List<LessonForm> lessons;
    private List<LecturerForm> lecturers;
    private List<UserFormForCollections> users;
    private List<TagForm> tags;

    public static Event convert(ExtendedEventForm eventForm) {
        return Event.builder()
                .name(eventForm.getName())
                .description(eventForm.getDescription())
                .address(eventForm.getAddress())
                .email(eventForm.getEmail())
                .phone(eventForm.getPhone())
                .lessonsCount(eventForm.getLessonsCount())
                .beginTime(eventForm.getBeginTime())
                .endTime(eventForm.getEndTime())
                .point(eventForm.getPoint())
                .build();
    }
}