package com.onlinecourses.services;

import com.onlinecourses.forms.ExtendedUserForm;
import com.onlinecourses.forms.UserForm;
import com.onlinecourses.models.User;
import com.onlinecourses.transfer.ExtendedUserDto;
import com.onlinecourses.transfer.UserDto;

import java.util.List;
import java.util.Optional;

public interface UsersService {
    List<UserDto> findAllByEvent(Long eventId);
    List<UserDto> findAllByCity(String city);
    List<UserDto> findAllByName(String name);
    List<UserDto> findAllByLogin(String login);

    Optional<User> findOneByLogin(String login);
    Optional<User> findOneByToken(String token);
    Optional<User> save(UserForm userForm);

    Optional<ExtendedUserDto> findOneById(Long id);
    Optional<ExtendedUserDto> update(ExtendedUserForm userForm, User user, Long userId);

    void signUp(UserForm userForm);
    void delete(Long id);
}
