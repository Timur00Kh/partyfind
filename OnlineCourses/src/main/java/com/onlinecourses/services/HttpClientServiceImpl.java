package com.onlinecourses.services;

import com.onlinecourses.utils.PropPathUtil;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class HttpClientServiceImpl implements HttpClientService {

    @Autowired
    private PropPathUtil propPathUtil;

    @Override
    @SneakyThrows
    public byte[] getFile(String dirname, String filename) {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(propPathUtil.getProperty("static-files.host") + ":" + propPathUtil.getProperty("nodejs.port")
                + "/" +propPathUtil.getProperty("static-files.files.prefix") + "/" + dirname + "/" + filename);
        return IOUtils.toByteArray(client.execute(request).getEntity().getContent());

    }

    @Override
    @SneakyThrows
    public byte[] getUserPhoto(String filename) {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(propPathUtil.getProperty("static-files.host") + ":" + propPathUtil.getProperty("nodejs.port")
                + "/" + propPathUtil.getProperty("static-files.userPhotos.prefix") + "/" + filename);
        return IOUtils.toByteArray(client.execute(request).getEntity().getContent());
    }

    @Override
    @SneakyThrows
    public List<String> uploadFiles(List<MultipartFile> files) {
        HttpPost post = new HttpPost(propPathUtil.getProperty("static-files.host") + ":" + propPathUtil.getProperty("nodejs.port") + "/upload");
        List<String> list = new ArrayList<>();
        String dirname = UUID.randomUUID().toString().replace("-", "").substring(0, 15);
        post.addHeader("dir", dirname);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        for (MultipartFile multipartFile : files) {
            list.add("/" + propPathUtil.getProperty("static-files.files.prefix") + "/" + dirname + "/" + multipartFile.getOriginalFilename());
            File file = new File(multipartFile.getOriginalFilename());
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(multipartFile.getBytes());
            fos.close();
            builder.addPart("files", new FileBody(file, ContentType.DEFAULT_BINARY));
        }
        HttpEntity entity = builder.build();
        post.setEntity(entity);
        new DefaultHttpClient().execute(post);
        return list;
    }

    @Override
    @SneakyThrows
    public String uploadUserPhoto(MultipartFile multipartFile) {
        return uploadPhotoCrop(propPathUtil.getProperty("static-files.userPhotos.prefix"), multipartFile);
    }

    @Override
    @SneakyThrows
    public byte[] getEventPhoto(String filename) {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(propPathUtil.getProperty("static-files.host") + ":" + propPathUtil.getProperty("nodejs.port")
                + "/" + propPathUtil.getProperty("static-files.eventPhotos.prefix") + "/" + filename);
        return IOUtils.toByteArray(client.execute(request).getEntity().getContent());
    }

    @Override
    @SneakyThrows
    public byte[] getEventHeader(String filename) {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(propPathUtil.getProperty("static-files.host") + ":" + propPathUtil.getProperty("nodejs.port")
                + "/" + propPathUtil.getProperty("static-files.eventHeader.prefix") + "/" + filename);
        return IOUtils.toByteArray(client.execute(request).getEntity().getContent());
    }

    @Override
    public String uploadEventPhoto(MultipartFile multipartFile) {
        return uploadPhotoCrop(propPathUtil.getProperty("static-files.eventPhotos.prefix"), multipartFile);
    }

    @Override
    public String uploadEventHeader(MultipartFile multipartFile) {
        return uploadPhotoNotCrop(propPathUtil.getProperty("static-files.eventHeader.prefix"), multipartFile);
    }

    @SneakyThrows
    private String uploadPhotoCrop(String dir, MultipartFile multipartFile) {
        HttpPost post = new HttpPost(propPathUtil.getProperty("static-files.host") + ":" + propPathUtil.getProperty("nodejs.port") + "/uploadPhoto");
        post.addHeader("dir", dir);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        byte[] bytes = multipartFile.getBytes();
        File file = new File(multipartFile.getName());
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
        stream.write(bytes);
        stream.close();
        BufferedImage source = ImageIO.read(file);
        BufferedImage image200;
        if (source.getHeight() > source.getWidth())
            image200 = Scalr.resize(source, Scalr.Mode.FIT_TO_WIDTH, 200);
        else
            image200 = Scalr.resize(source, Scalr.Mode.FIT_TO_HEIGHT, 200);
        if (source.getHeight() != source.getWidth())
            image200 = Scalr.crop(image200, 200, 200);

        File image = createImage(image200);

        builder.addPart("files", new FileBody(image, ContentType.DEFAULT_BINARY));
        post.setEntity(builder.build());
        new DefaultHttpClient().execute(post);
        return "/" + dir + "/" + image.getName();
    }

    @SneakyThrows
    private String uploadPhotoNotCrop(String dir, MultipartFile multipartFile) {
        HttpPost post = new HttpPost(propPathUtil.getProperty("static-files.host") + ":" + propPathUtil.getProperty("nodejs.port") + "/uploadPhoto");
        post.addHeader("dir", dir);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        File file = new File(multipartFile.getOriginalFilename());
        file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(multipartFile.getBytes());
        fos.close();
        File image = createImage(ImageIO.read(file));
        builder.addPart("files", new FileBody(image, ContentType.DEFAULT_BINARY));
        post.setEntity(builder.build());
        new DefaultHttpClient().execute(post);
        return "/" + dir + "/" + image.getName();

    }

    @SneakyThrows
    private File createImage(BufferedImage image200){
        BufferedImage image200Buff = new BufferedImage(image200.getWidth(),
                image200.getHeight(), BufferedImage.TYPE_INT_RGB);
        image200Buff.createGraphics().drawImage(image200, 0, 0, Color.WHITE, null);
        String filename = UUID.randomUUID().toString().replace("-", "").substring(0, 25) + ".jpg";
        File file = new File(filename);
        ImageIO.write(image200Buff, "jpg", file);
        return file;
    }


}
