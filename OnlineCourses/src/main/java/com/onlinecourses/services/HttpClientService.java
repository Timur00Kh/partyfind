package com.onlinecourses.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

public interface HttpClientService {
    byte[] getFile(String dirname, String filename);
    byte[] getUserPhoto(String filename);
    byte[] getEventPhoto(String filename);
    byte[] getEventHeader(String filename);

    String uploadUserPhoto(MultipartFile file);
    String uploadEventPhoto(MultipartFile file);
    String uploadEventHeader(MultipartFile file);

    List<String> uploadFiles(List<MultipartFile> files);
}
