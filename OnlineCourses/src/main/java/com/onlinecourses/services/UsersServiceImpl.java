package com.onlinecourses.services;

import com.onlinecourses.forms.ExtendedUserForm;
import com.onlinecourses.forms.UserForm;
import com.onlinecourses.models.Event;
import com.onlinecourses.models.Role;
import com.onlinecourses.models.State;
import com.onlinecourses.models.User;
import com.onlinecourses.repositories.EventsRepository;
import com.onlinecourses.repositories.UsersRepository;
import com.onlinecourses.transfer.ExtendedUserDto;
import com.onlinecourses.transfer.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private EventsRepository eventsRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<UserDto> findAllByEvent(Long eventId) {
        Optional<Event> event = eventsRepository.findById(eventId);
        return event.map(event1 -> UserDto.from(usersRepository.findAllByEvent(event1.getId()))).orElse(null);
    }

    @Override
    public List<UserDto> findAllByCity(String city) {
        return UserDto.from(usersRepository.findAllByCity(city));
    }

    @Override
    public List<UserDto> findAllByName(String name) {
        return UserDto.from(usersRepository.findAllByName(name));
    }

    @Override
    public List<UserDto> findAllByLogin(String login) {
        return UserDto.from(usersRepository.findAllByLogin(login));
    }

    @Override
    public Optional<User> findOneByLogin(String login) {
        return usersRepository.findOneByLogin(login);
    }

    @Override
    public Optional<ExtendedUserDto> findOneById(Long id) {
        Optional<User> user = usersRepository.findById(id);
        if (user.isPresent()) {
            return Optional.ofNullable(ExtendedUserDto.from(user.get()));
        } else return Optional.empty();
    }

    @Override
    public Optional<User> findOneByToken(String token) {
        return usersRepository.findByToken(token);
    }

    @Override
    public Optional<ExtendedUserDto> update(ExtendedUserForm userForm, User user, Long userId) {
        if (!user.getId().equals(userId)) {
            throw new IllegalArgumentException("Access denied:" + user.getLogin() + " tries to update another user");
        }

        User updateUser = ExtendedUserForm.convert(userForm, user);
        updateUser.setHashPassword(userForm.getPassword() == null ?
                user.getHashPassword() : passwordEncoder.encode(userForm.getPassword()));

        updateUser = usersRepository.save(updateUser);
        return Optional.ofNullable(ExtendedUserDto.from(updateUser));
    }

    @Override
    public Optional<User> save(UserForm userForm) {
        String password = passwordEncoder.encode(userForm.getPassword());

        User user = User.builder()
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .login(userForm.getLogin())
                .email(userForm.getEmail())
                .hashPassword(password)
                .build();

        return Optional.of(usersRepository.save(user));
    }

    @Override
    public void signUp(UserForm userForm) {
        String hashPassword = passwordEncoder.encode(userForm.getPassword());

        User user = User.builder()
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .hashPassword(hashPassword)
                .login(userForm.getLogin())
                .role(Role.USER)
                .state(State.ACTIVE)
                .build();

        usersRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        usersRepository.deleteById(id);
    }
}