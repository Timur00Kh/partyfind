package com.onlinecourses.services;

import com.onlinecourses.models.Tag;
import com.onlinecourses.repositories.TagRepository;
import com.onlinecourses.transfer.TagDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public List<TagDto> findAll() {
        return TagDto.from(tagRepository.findAll());
    }
}
