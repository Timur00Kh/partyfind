package com.onlinecourses.services;

import com.onlinecourses.forms.*;
import com.onlinecourses.models.*;
import com.onlinecourses.repositories.*;
import com.onlinecourses.transfer.EventDto;
import com.onlinecourses.transfer.ExtendedEventDto;
import com.onlinecourses.transfer.SearchEventDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class EventsServiceImpl implements EventsService {

    @Autowired
    private EventsRepository eventsRepository;

    @Autowired
    private LessonsRepository lessonsRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private FileRepository filesRepository;

    @Autowired
    private LecturersRepository lecturersRepository;

    @Autowired
    private TagRepository tagsRepository;

    @Override
    public List<EventDto> findAll() {
        return EventDto.from(eventsRepository.findAll());
    }

    @Override
    public List<SearchEventDto> findAllByName(String name) {
        return SearchEventDto.from(eventsRepository.findAllByName(name));
    }

    @Override
    public List<EventDto> findAllByAddress(String address) {
        return EventDto.from(eventsRepository.findAllByAddress(address));
    }

    @Override
    public List<EventDto> findAllByBeginTime(LocalDateTime beginTime) {
        return EventDto.from(eventsRepository.findAllByBeginTime(beginTime));
    }

    @Override
    public List<EventDto> findAllByEndTime(LocalDateTime endTime) {
        return EventDto.from(eventsRepository.findAllByEndTime(endTime));
    }

    @Override
    public List<EventDto> findAllByBeginTimeAndEndTime(LocalDateTime beginTime, LocalDateTime endTime) {
        return EventDto.from(eventsRepository.findAllByBeginTimeAndEndTime(beginTime, endTime));
    }

    @Override
    public List<SearchEventDto> findAllByFilters(String eventName, String tags) {
        tags = tags == null ? null : tags.equals("") ? null : tags;
        if (eventName == null && tags == null) {
            return SearchEventDto.from(eventsRepository.findAll());
        }

        if (tags == null) {
            return SearchEventDto.from(eventsRepository.findAllByName(eventName));
        } else {
            return SearchEventDto.from(eventsRepository.
                    findAllByFilers(eventName == null ? "" : eventName, tags, ",", "0"));
        }
    }

    @Override
    public Optional<ExtendedEventDto> findOne(Long id) {
        Optional<Event> event = eventsRepository.findById(id);
        if (event.isPresent()) {
            return Optional.ofNullable(ExtendedEventDto.from(event.get()));
        } else return Optional.empty();
    }

    @Override
    public Optional<ExtendedEventDto> findOneByUser(Long eventId, User user) {
        Event event = eventsRepository.findById(eventId).orElse(null);
        ExtendedEventDto eventDto = ExtendedEventDto.from(Objects.requireNonNull(event));

        if (user != null && usersRepository.findByUserAndEvent(user.getId(), eventId) != null) {
            eventDto.setSubscriber(true);
        }

        return Optional.ofNullable(eventDto);
    }

    @Override
    public void checkSubscriptionByUser(Long eventId, User user, String action) {
        final String subscribe = "subscribe";
        final String unsubscribe = "unsubscribe";

        action = action == null ? null : action.equals("") ? null : action;

        if (action != null && user != null) {
            if (action.equals(subscribe)) {
                eventsRepository.subscribeUserByFunction(eventId, user.getId());
            }
            if (action.equals(unsubscribe)) {
                eventsRepository.unsubscribeUserById(eventId, user.getId());
            }
        }
    }

    @Override
    public Optional<ExtendedEventDto> save(EventForm eventForm, User creator) {
        Event event = EventForm.convert(eventForm);
        event = eventsRepository.save(event);

        event.setLessons(saveLessons(eventForm.getLessons(), event));
        saveFiles(event.getLessons());
        event.setLecturers(saveLecturers(eventForm.getLecturers(), event));
        event.setCreator(creator);

        return Optional.ofNullable(ExtendedEventDto.from(eventsRepository.save(event)));
    }

    private List<Lesson> saveLessons(List<LessonForm> lessonForms, Event event) {
        List<Lesson> lessons = null;

        if (lessonForms != null) {

            lessons = LessonForm.convert(lessonForms);

            for (Lesson lesson : lessons) {
//                if (lesson.getId() != null) {
//                    Optional<Lesson> lessonCandidate = lessonsRepository.findById(lesson.getId());
//                    if (lessonCandidate.isPresent()) {
//                        lesson = lessonCandidate.get();
//                    } else continue;
//                }
                lesson.setEvent(event);
                for (int i = 0; lesson.getFiles() != null && i < lesson.getFiles().size(); i++) {
                    FileModel fileModel = lesson.getFiles().get(i);
                    fileModel.setLesson(lesson);
                }
            }

            lessonsRepository.saveAll(lessons);
        }
        return lessons;
    }

    private void saveFiles(List<Lesson> lessons) {
        ArrayList<FileModel> files = new ArrayList<>();

        if (lessons != null) {
            for (Lesson lesson : lessons) {
                files.addAll(lesson.getFiles());
            }

            filesRepository.saveAll(files);
        }
    }

    private List<Lecturer> saveLecturers(List<LecturerForm> lecturerForms, Event event) {
        List<Lecturer> lecturers = new ArrayList<>();

        if (lecturerForms != null) {
            for (Lecturer lecturer : LecturerForm.convert(lecturerForms)) {
                if (lecturer.getId() != null) {
                    Optional<Lecturer> lecturerCandidate = lecturersRepository.findById(lecturer.getId());
                    if (lecturerCandidate.isPresent()) {
                        lecturer = lecturerCandidate.get();
                    } else continue;
                } else continue;

                if (lecturer.getEvents() == null) {
                    lecturer.setEvents(new ArrayList<>());
                }
                lecturer.getEvents().add(event);
                lecturers.add(lecturer);
            }

            lecturersRepository.saveAll(lecturers);
        }
        return lecturers;
    }

    @Override
    public Optional<ExtendedEventDto> update(ExtendedEventForm eventForm, User user, Long eventId) {
        Optional<Event> eventCandidate = eventsRepository.findById(eventId);

        if (eventCandidate.isPresent() &&
                (user.getRole().equals(Role.ADMIN) || eventCandidate.get().getCreator().getId().equals(user.getId()))) {
            Event event = ExtendedEventForm.convert(eventForm);
            event.setId(eventId);
            event.setCreator(user);

            event.setLessons(saveLessons(eventForm.getLessons(), event));
            saveFiles(event.getLessons());
            event.setLecturers(saveLecturers(eventForm.getLecturers(), event));
            event.setTags(receiveTags(eventForm.getTags()));

            return Optional.ofNullable(ExtendedEventDto.from(eventsRepository.save(event)));
        } else throw new IllegalArgumentException("Access denied, user is not ADMIN or creator of event");
    }

    private List<Tag> receiveTags(List<TagForm> tagForms) {
        List<Tag> tags = new ArrayList<>();

        for (TagForm form: tagForms) {
            Optional<Tag> tag = tagsRepository.findById(form.getId());
            tag.ifPresent(tags::add);
        }

        return tags;
    }

    @Override
    public void delete(long eventId, User user) {
        eventsRepository.deleteEventByIdAndCreator(eventId, user.getId());
    }
}