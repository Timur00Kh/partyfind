package com.onlinecourses.services;

import com.onlinecourses.forms.EventForm;
import com.onlinecourses.forms.ExtendedEventForm;
import com.onlinecourses.models.*;
import com.onlinecourses.transfer.EventDto;
import com.onlinecourses.transfer.ExtendedEventDto;
import com.onlinecourses.transfer.SearchEventDto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface EventsService {
    List<SearchEventDto> findAllByFilters(String eventName, String tagsId);
    List<SearchEventDto> findAllByName(String name);

    List<EventDto> findAll();
    List<EventDto> findAllByAddress(String address);
    List<EventDto> findAllByBeginTime(LocalDateTime beginTime);
    List<EventDto> findAllByEndTime(LocalDateTime endTime);
    List<EventDto> findAllByBeginTimeAndEndTime(LocalDateTime beginTime, LocalDateTime endTime);

    Optional<ExtendedEventDto> findOne(Long id);
    Optional<ExtendedEventDto> findOneByUser(Long id, User user);
    Optional<ExtendedEventDto> save(EventForm eventForm, User user);
    Optional<ExtendedEventDto> update(ExtendedEventForm event, User user, Long eventId);

    void delete(long id, User user);
    void checkSubscriptionByUser(Long eventId, User user, String action);
}
