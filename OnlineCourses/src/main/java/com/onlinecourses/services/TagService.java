package com.onlinecourses.services;

import com.onlinecourses.transfer.TagDto;

import java.util.List;

public interface TagService {
    List<TagDto> findAll();
}
