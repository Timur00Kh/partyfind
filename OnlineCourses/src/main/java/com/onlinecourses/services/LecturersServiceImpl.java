package com.onlinecourses.services;

import com.onlinecourses.forms.EventFormForCollections;
import com.onlinecourses.forms.ExtendedLecturerForm;
import com.onlinecourses.forms.LecturerForm;
import com.onlinecourses.forms.TagForm;
import com.onlinecourses.models.*;
import com.onlinecourses.repositories.*;
import com.onlinecourses.transfer.ExtendedLecturerDto;
import com.onlinecourses.transfer.LecturerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.Optional;

@Service
public class LecturersServiceImpl implements LecturersService{
    @Autowired
    private LecturersRepository lecturersRepository;

    @Autowired
    private EventsRepository eventsRepository;

    @Autowired
    private TagRepository tagsRepository;

    @Override
    public List<LecturerDto> findAllByEvent(Long eventId) {
        return LecturerDto.from(lecturersRepository.findAllByEvent(eventId));
    }

    @Override
    public List<LecturerDto> findAll() {
        return LecturerDto.from(lecturersRepository.findAll());
    }

    @Override
    public List<LecturerDto> findAllByName(String name) {
        return LecturerDto.from(lecturersRepository.findAllByName(name));
    }

    @Override
    public Optional<ExtendedLecturerDto> findOne(Long id) {
        Optional<Lecturer> lecturer = lecturersRepository.findById(id);
        if (lecturer.isPresent()) {
            return Optional.ofNullable(ExtendedLecturerDto.from(lecturer.get()));
        } else return Optional.empty();
    }

    @Override
    public Optional<ExtendedLecturerDto> save(LecturerForm lecturerForm) {
        Lecturer lecturer = LecturerForm.convert(lecturerForm);
        return Optional.ofNullable(ExtendedLecturerDto.from(lecturersRepository.save(lecturer)));
    }

    @Override
    public Optional<ExtendedLecturerDto> update(ExtendedLecturerForm lecturerForm, Long lecturerId) {
        Optional<Lecturer> lecturer = lecturersRepository.findById(lecturerId);
        if (lecturer.isPresent()) {
            Lecturer updateLecturer = ExtendedLecturerForm.convert(lecturerForm, lecturer.get());

            updateLecturer.setTags(receiveTags(lecturerForm.getTags()));
            updateLecturer.setEvents(receiveEvents(lecturerForm.getEvents()));

            return Optional.ofNullable(ExtendedLecturerDto.from(lecturersRepository.save(updateLecturer)));
        } else throw new IllegalArgumentException("No lecturer with such id: " + lecturerId);
    }

    private List<Tag> receiveTags(List<TagForm> tagForms) {
        List<Tag> tags = new ArrayList<>();

        for (TagForm form: tagForms) {
            Optional<Tag> tag = tagsRepository.findById(form.getId());
            tag.ifPresent(tags::add);
        }

        return tags;
    }

    private List<Event> receiveEvents(List<EventFormForCollections> eventForms) {
        List<Event> events = new ArrayList<>();

        for (EventFormForCollections form: eventForms) {
            Optional<Event> event = eventsRepository.findById(form.getId());
            event.ifPresent(events::add);
        }

        return events;
    }

    @Override
    public void delete(Long id) {
        lecturersRepository.deleteById(id);
    }

    @Override
    public List<LecturerDto> findAllByFilters(String name, String tags) {
        tags = tags == null ? null : tags.equals("") ? null : tags;
        if (name == null && tags == null) {
            return LecturerDto.from(lecturersRepository.findAll());
        }
        if (tags == null) {
            return LecturerDto.from(lecturersRepository.findAllByName(name));
        } else {
            return LecturerDto.from(lecturersRepository.
                    findAllByFilers(name == null ? "" : name, tags, ",", "0"));
        }
    }
}