package com.onlinecourses.services;

import com.onlinecourses.forms.ExtendedLecturerForm;
import com.onlinecourses.forms.LecturerForm;
import com.onlinecourses.transfer.ExtendedLecturerDto;
import com.onlinecourses.transfer.LecturerDto;

import java.util.List;
import java.util.Optional;

public interface LecturersService {
    List<LecturerDto> findAllByEvent(Long eventId);
    List<LecturerDto> findAll();
    List<LecturerDto> findAllByName(String name);
    List<LecturerDto> findAllByFilters(String name, String tagsId);

    Optional<ExtendedLecturerDto> findOne(Long id);
    Optional<ExtendedLecturerDto> save(LecturerForm lecturerForm);
    Optional<ExtendedLecturerDto> update(ExtendedLecturerForm lecturerForm, Long lecturerId);

    void delete(Long id);
}
