package com.onlinecourses.services;

import com.onlinecourses.forms.UserForm;

public interface SignUpService {
    void signUp(UserForm userForm);
}
