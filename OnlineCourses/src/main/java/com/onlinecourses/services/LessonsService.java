package com.onlinecourses.services;

import com.onlinecourses.forms.LessonForm;
import com.onlinecourses.models.FileModel;
import com.onlinecourses.models.FileModel;
import com.onlinecourses.models.Lesson;
import com.onlinecourses.transfer.LessonDto;

import java.util.List;

public interface LessonsService {
    List<LessonDto> findAllByEventId(Long eventId);
    List<LessonDto> findAllByName(String name);

    Lesson save(LessonForm lessonForm);
    Lesson update(LessonForm lesson);

    List<FileModel> findFilesByOneLesson(Long lessonId);
    List<FileModel> findFilesByLessons(List<Lesson> lessons);

    void delete(Long lessonId);
}
