package com.onlinecourses.services;

import com.onlinecourses.forms.LessonForm;
import com.onlinecourses.models.Event;
import com.onlinecourses.models.FileModel;
import com.onlinecourses.models.FileModel;
import com.onlinecourses.models.Lesson;
import com.onlinecourses.repositories.EventsRepository;
import com.onlinecourses.repositories.FileRepository;
import com.onlinecourses.repositories.LessonsRepository;
import com.onlinecourses.transfer.LessonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LessonsServiceImpl implements LessonsService {

    @Autowired
    private LessonsRepository lessonsRepository;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private EventsRepository eventsRepository;

    @Override
    public List<LessonDto> findAllByEventId(Long eventId) {
        return LessonDto.from(lessonsRepository.findAllByEvent(eventId));
    }

    @Override
    public List<LessonDto> findAllByName(String name) {
        return LessonDto.from(lessonsRepository.findAllByName(name));
    }

    @Override
    public Lesson save(LessonForm lessonForm) {
        Optional<Event> event = eventsRepository.findById(lessonForm.getEventId());

        Lesson lesson = LessonForm.convert(lessonForm);
        lesson.setEvent(event.orElse(null));

        lesson = lessonsRepository.save(lesson);

        for (FileModel fileModel: lesson.getFiles()) {
            fileModel.setLesson(lesson);
        }

        fileRepository.saveAll(lesson.getFiles());

        return lesson;
    }

    @Override
    public Lesson update(LessonForm lesson) {
        return save(lesson);
    }

    @Override
    public List<FileModel> findFilesByOneLesson(Long lessonId) {
        return fileRepository.findAllByOneLesson(lessonId);
    }

    @Override
    public List<FileModel> findFilesByLessons(List<Lesson> lessons) {
        return fileRepository.findAllByLessons(lessons.stream().map(Lesson::getId).collect(Collectors.toList()));
    }

    @Override
    public void delete(Long lessonId) {
        lessonsRepository.deleteById(lessonId);
    }
}