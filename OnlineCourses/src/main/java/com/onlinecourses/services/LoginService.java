package com.onlinecourses.services;

import com.onlinecourses.forms.LoginForm;
import com.onlinecourses.transfer.TokenDto;

public interface LoginService {
    TokenDto login(LoginForm loginForm);
}
