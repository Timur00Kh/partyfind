package com.onlinecourses.security.filters;

import com.onlinecourses.security.token.TokenAuthentication;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class TokenAuthFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        String token = httpServletRequest.getParameter("token");

        TokenAuthentication tokenAuthentication = new TokenAuthentication(token);

        if (token == null) {
            tokenAuthentication.setAuthenticated(false);
            tokenAuthentication.setUserDetails(null);
            SecurityContextHolder.getContext().setAuthentication(tokenAuthentication);
        } else {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication == null || !authentication.isAuthenticated() || !authentication.getName().equals(token)) {
                SecurityContextHolder.getContext().setAuthentication(tokenAuthentication);
            }
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
