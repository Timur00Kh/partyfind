package com.onlinecourses.security.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationEventListener implements ApplicationListener<AbstractAuthenticationEvent> {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onApplicationEvent(AbstractAuthenticationEvent event) {
        if (event instanceof InteractiveAuthenticationSuccessEvent) {
            return;
        }
        Authentication authentication = event.getAuthentication();

        if (authentication != null && authentication.getPrincipal() != null) {
            String auditMessage = "Login attempt: " +
                    authentication.getName() + " " +
                    ((authentication.getPrincipal() == null) ? null : authentication.getAuthorities()) + " " +
                    authentication.getCredentials() + " " +
                    authentication.getPrincipal() + " " +
                    "\nStatus: " + authentication.isAuthenticated();
            logger.info(auditMessage);
        }
    }
}