package com.onlinecourses.security.details;

import com.onlinecourses.models.Token;
import com.onlinecourses.repositories.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class  UserDetailsServiceImpl implements UserDetailsService {

    private TokenRepository tokenRepository;

    @Autowired
    public UserDetailsServiceImpl(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public UserDetailsImpl loadUserByUsername(String token) throws UsernameNotFoundException {
        Optional<Token> tokenCandidate = tokenRepository.findFirstByValue(token);

        if (tokenCandidate.isPresent() && tokenCandidate.get().isNotExpired()) {
            return new UserDetailsImpl(tokenCandidate.get().getUser());
        } else return null;
    }
}
