package com.onlinecourses.security.config;

import com.onlinecourses.security.filters.TokenAuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@ComponentScan("com.onlinecourses")
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    private TokenAuthFilter tokenAuthFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .addFilterBefore(tokenAuthFilter, BasicAuthenticationFilter.class)
                .antMatcher("/**")
                .authenticationProvider(authenticationProvider)
                .authorizeRequests()
                .antMatchers("/signUp").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers(HttpMethod.POST,"/event").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.PUT, "/event/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.DELETE, "/event/**").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/event/*/subscription/**").hasAnyAuthority("USER","ADMIN")
                .antMatchers(HttpMethod.GET, "/event/**").permitAll()
                .antMatchers("/events").permitAll()
                .antMatchers(HttpMethod.POST, "/lecturer").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.DELETE, "/lecturer/*").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.PUT, "/lecturer/*").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.PUT, "/user/*").hasAnyAuthority("USER", "ADMIN");
        http.authorizeRequests().antMatchers("/swagger-ui.html#/**").permitAll();
        http.csrf().disable();
    }

}