package com.onlinecourses.repositories;

import com.onlinecourses.models.Lecturer;
import com.onlinecourses.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LecturersRepository extends JpaRepository<Lecturer, Long> {
    @Query(nativeQuery = true,
            value = "SELECT * from lecturer where  id in (select lecturer_id from lecturer_event where event_id = ?1)")
    List<Lecturer> findAllByEvent(Long eventId);

    @Query(nativeQuery = true,
            value = "select * from lecturer where lower(coalesce(first_name, ' ') || ' ' || coalesce(last_name, ' ')) like lower('%' || ?1 || '%')")
    List<Lecturer> findAllByName(String name);

    @Query(nativeQuery = true,
            value = "with recursive groupByLecturers(lecturer, tags) as " +
                    "    ( " +
                    "    select lecturer_id as lecturer, cast(array[] as bigint[]) as tags " +
                    "    from lecturer_tag " +
                    "        union " +
                    "        select lecturer_id, " +
                    "               groupByLecturers.tags || tag_id as tags " +
                    "        from lecturer_tag " +
                    "               join groupByLecturers on " +
                    "                lecturer_id = event and array_position(tags, tag_id) isnull " +
                    "               and lecturer_id in (select * from sortLecturers) " +
                    "    ), " +
                    "               getTags as ( " +
                    "    select distinct on (lecturer) lecturer, tags from groupByLecturers " +
                    "    where cardinality(tags) = (select count(*) from lecturer_tag where lecturer_id = lecturer) " +
                    "    ), " +
                    "               findLecturers as " +
                    "    ( " +
                    "    select * from lecturer " +
                    "    where " +
                    "        id in ( " +
                    "              select lecturer from getTags " +
                    "              where cast(tags as bigint[]) @> cast(string_to_array(:tagsString, :separator, :replacer) as bigint[]) " +
                    "              ) " +
                    "    ), " +
                    "    sortLecturers as ( " +
                    "     select id from lecturer where lower(coalesce(first_name, ' ') || ' ' || coalesce(last_name, ' ')) like lower('%' || :lecturerName || '%') " +
                    "               and (select count(*) from lecturer_tag where id = lecturer_id) >= cardinality(string_to_array(:tagsString, :separator, :replacer)) " +
                    "    ) " +
                    "select * from findLecturers")
    List<Lecturer> findAllByFilers(@Param("lecturerName") String lecturerName,
                                   @Param("tagsString") String tags,
                                   @Param("separator") String separator,
                                   @Param("replacer") String replacer);
}