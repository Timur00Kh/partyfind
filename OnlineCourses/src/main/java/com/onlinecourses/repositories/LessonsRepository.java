package com.onlinecourses.repositories;

import com.onlinecourses.models.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LessonsRepository extends JpaRepository<Lesson, Long> {
    @Query(nativeQuery = true, value = "select * from lesson where event_id = ?1;")
    List<Lesson> findAllByEvent(Long eventId);

    @Query(nativeQuery = true,
            value = "select * from  lesson where name like ('%' || ?1 || '%')")
    List<Lesson> findAllByName(String name);
}