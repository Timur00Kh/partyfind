package com.onlinecourses.repositories;

import com.onlinecourses.models.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

public interface TokenRepository extends JpaRepository<Token, Long> {
    Optional<Token> findFirstByValue(String value);

    @Transactional
    void deleteTokensByExpiredDateTimeBefore(LocalDateTime currentTime);
}
