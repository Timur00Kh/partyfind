package com.onlinecourses.repositories;

import com.onlinecourses.models.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

public interface EventsRepository extends JpaRepository<Event, Long> {
    List<Event> findAll();
    List<Event> findAllByAddress(String address);
    List<Event> findAllByBeginTime(LocalDateTime beginTime);
    List<Event> findAllByEndTime(LocalDateTime endTime);

    @Query(nativeQuery = true,
            value = "with recursive groupByEvents(event, tags) as " +
                    "    ( " +
                    "    select event_id as event, cast(array[] as bigint[]) as tags " +
                    "    from event_tag " +
                    "        union " +
                    "        select event_id, " +
                    "               groupByEvents.tags || tag_id as tags " +
                    "        from event_tag " +
                    "               join groupByEvents on " +
                    "                event_id = event and array_position(tags, tag_id) isnull " +
                    "               and event_id in (select * from sortEvents) " +
                    "    ), " +
                    "               getTags as ( " +
                    "    select distinct on (event) event, tags from groupByEvents " +
                    "    where cardinality(tags) = (select count(*) from event_tag where event_id = event) " +
                    "    ), " +
                    "               findEvents as " +
                    "    ( " +
                    "    select * from cs_event " +
                    "    where " +
                    "        id in ( " +
                    "              select event from getTags " +
                    "              where cast(tags as bigint[]) @> cast(string_to_array(:tagsString, :separator, :replacer) as bigint[]) " +
                    "              ) " +
                    "    ), " +
                    "    sortEvents as ( " +
                    "     select id from cs_event where lower(coalesce(name, ' ')) like lower('%' || :eventName || '%') " +
                    "               and (select count(*) from event_tag where id = event_id) >= cardinality(string_to_array(:tagsString, :separator, :replacer)) " +
                    "    ) " +
                    "select * from findEvents")
    List<Event> findAllByFilers(@Param("eventName") String eventName,
                                @Param("tagsString") String tags,
                                @Param("separator") String separator,
                                @Param("replacer") String replacer);

    @Query(nativeQuery = true, value = "SELECT * from cs_event where begin_time <= ?1 and end_time >= ?2")
    List<Event> findAllByBeginTimeAndEndTime(LocalDateTime beginTime, LocalDateTime endTime);

    @Query(nativeQuery = true, value = "select * from cs_event where lower(coalesce(name, ' ')) like lower('%' || ?1 || '%')")
    List<Event> findAllByName(String name);

    @Query(nativeQuery = true, value = "select * from cs_event where lower(coalesce(name, ' ')) like lower('%' || ?1 || '%') " +
            "and (select count(*) from event_tag where event_id = cs_event.id) >= ?2")
    List<Event> findAllByNameAndTagCount(String name, Long count);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "insert into user_event(user_id, event_id) values (?2, ?1)")
    void subscribeByUser(Long eventId, Long userId);

    @Query(nativeQuery = true, value = "select subscribeUser(?1, ?2)")
    void subscribeUserByFunction(Long eventId, Long userId);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "delete from user_event where " +
                    "event_id = ?1 and user_id = ?2")
    void unsubscribeUserById(Long eventId, Long userId);

    Optional<Event> findById(Long id);

    @Query(nativeQuery = true, value = "select id from cs_event where id = ?1")
    Long findEventIdById(Long id);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "delete from cs_event where id = ?1 and " +
            "(exists(select id from cs_event where creator_id = ?2) or  (select role from cs_user where cs_user.id = ?2) = 'ADMIN')")
    void deleteEventByIdAndCreator(Long eventId, Long creatorId);
}