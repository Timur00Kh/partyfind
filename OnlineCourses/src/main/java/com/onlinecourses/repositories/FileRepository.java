package com.onlinecourses.repositories;

import com.onlinecourses.models.FileModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface FileRepository extends JpaRepository<FileModel, Long> {

    @Query(nativeQuery = true,
            value = "select * from file where " +
                    "id in (select file_id from file_lesson where lesson_id in (:lessons))")
    List<FileModel> findAllByLessons(@Param(value = "lessons") List<Long> lessons);

    @Query(nativeQuery = true,
            value = "select * from file where " +
                    "id in (select file_id from file_lesson where lesson_id = ?1)")
    List<FileModel> findAllByOneLesson(Long lessonId);

}
