package com.onlinecourses.repositories;

import com.onlinecourses.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    @Query(nativeQuery = true, value = "select * from cs_user where exists(select 1 from user_event where event_id = ?1 and user_id = cs_user.id limit 1);")
    List<User> findAllByEvent(Long eventId);

    List<User> findAllByCity(String city);

    @Query(nativeQuery = true, value = "select * from cs_user where login like ('%' || ?1 || '%')")
    List<User> findAllByLogin(String login);

    @Query(nativeQuery = true,
            value = "select * from cs_user where (first_name || ' ' || last_name) like ('%' || ?1 || '%')")
    List<User> findAllByName(String name);

    @Query(nativeQuery = true,
            value = "select * from cs_user where id in (select user_id from token where value = ?1)")
    Optional<User> findByToken(String token);

    Optional<User> findOneByLogin(String login);

    @Query(nativeQuery = true, value = "select * from cs_user where id = (select lecturer.id from lecturer where user_id = ?1)")
    Optional<User> findByLecturer(Long lecturerId);

    @Query(nativeQuery = true,
            value = "select id from cs_user where id in " +
                    "(select user_id from user_event where " +
                    "user_id = ?1 " +
                    "and event_id in (select id from cs_event where id = ?2))")
    Long findByUserAndEvent(Long userId, Long eventId);
 }
