package com.onlinecourses.repositories;

import com.onlinecourses.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Long> {
    List<Tag> findAllById(List<Long> id);
    List<Tag> findAll();
}
