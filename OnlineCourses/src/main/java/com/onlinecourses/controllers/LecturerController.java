package com.onlinecourses.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.forms.ExtendedLecturerForm;
import com.onlinecourses.forms.LecturerForm;
import com.onlinecourses.models.View;
import com.onlinecourses.security.details.UserDetailsImpl;
import com.onlinecourses.services.LecturersService;
import com.onlinecourses.transfer.ExtendedLecturerDto;
import com.onlinecourses.transfer.LecturerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class LecturerController {

    @Autowired
    private LecturersService lecturersService;

    @JsonView(value = View.SearchLecturer.class)
    @GetMapping("/lecturers")
    public List<LecturerDto> getLecturers(){
        return lecturersService.findAll();
    }

    @JsonView(value = View.ExtendedLecturer.class)
    @GetMapping("/lecturer/{id}")
    public ResponseEntity<ExtendedLecturerDto> findLecturerById(@PathVariable("id") Long lecturerId ){
        return ResponseEntity.of(lecturersService.findOne(lecturerId));
    }

    @JsonView(value = View.SearchLecturer.class)
    @GetMapping(value = "/lecturer")
    public List<LecturerDto> findLecturersByFilters(
            @RequestParam(value = "name", required = false) String name, @RequestParam(value = "tags", required = false) String tags){
        return lecturersService.findAllByFilters(name, tags);
    }

    @JsonView(value = View.ExtendedLecturer.class)
    @PostMapping(value = "/lecturer")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ExtendedLecturerDto> createLecturer(@RequestBody LecturerForm lecturerForm){
        return ResponseEntity.of(lecturersService.save(lecturerForm));
    }

    @JsonView(value = View.ExtendedLecturer.class)
    @PutMapping(value = "/lecturer/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ExtendedLecturerDto> updateLecturer(@RequestBody ExtendedLecturerForm lecturerForm,
                                                              @PathVariable(name = "id") Long lecturerId) {
        return ResponseEntity.of(lecturersService.update(lecturerForm, lecturerId));
    }

    @DeleteMapping("/lecturer/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<Object> deleteLecturerById(@PathVariable("id") Long lecturerId ){
        lecturersService.delete(lecturerId);
        return ResponseEntity.ok().build();
    }
}