package com.onlinecourses.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.forms.LoginForm;
import com.onlinecourses.forms.UserForm;
import com.onlinecourses.models.User;
import com.onlinecourses.models.View;
import com.onlinecourses.services.LoginService;
import com.onlinecourses.services.UsersService;
import com.onlinecourses.transfer.TokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private UsersService usersService;

    @PostMapping("/login")
    public ResponseEntity<TokenDto> login(@RequestBody LoginForm loginForm) {
        return ResponseEntity.ok(loginService.login(loginForm));
    }

    @JsonView(value = {View.User.class})
    @PostMapping("/signUp")
    public ResponseEntity<Object> signUp(@RequestBody UserForm userForm) {
        usersService.signUp(userForm);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/signUp")
    public String getSignUpPage() {
        return "signUp";
    }

}