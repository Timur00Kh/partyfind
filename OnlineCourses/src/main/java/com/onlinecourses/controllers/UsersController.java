package com.onlinecourses.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.forms.ExtendedUserForm;
import com.onlinecourses.models.User;
import com.onlinecourses.models.View;
import com.onlinecourses.security.details.UserDetailsImpl;
import com.onlinecourses.services.UsersService;
import com.onlinecourses.transfer.ExtendedUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {

    @Autowired
    private UsersService usersService;

    @JsonView(value = View.ExtendedUser.class)
    @GetMapping(value = "/user/{id}")
    public ResponseEntity<ExtendedUserDto> findUserById(@PathVariable(value = "id") Long id){
        return ResponseEntity.of(usersService.findOneById(id));
    }

    @JsonView(value = View.ExtendedUser.class)
    @PostMapping(value = "/user")
    public ResponseEntity<User> findUserByToken(@AuthenticationPrincipal UserDetailsImpl userDetails) {
        return ResponseEntity.ok(userDetails.getUser());
    }

    @JsonView(value = View.ExtendedUser.class)
    @PutMapping(value = "/user/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ExtendedUserDto> updateUser(@RequestBody ExtendedUserForm userForm,
                                                      @AuthenticationPrincipal UserDetailsImpl userDetails,
                                                      @PathVariable(name = "id") Long userId) {
        return ResponseEntity.of(usersService.update(userForm, userDetails.getUser(), userId));
    }

}