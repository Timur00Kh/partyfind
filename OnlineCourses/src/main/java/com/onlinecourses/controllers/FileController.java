package com.onlinecourses.controllers;

import com.onlinecourses.services.HttpClientService;
import com.onlinecourses.utils.PropPathUtil;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
public class FileController {

    @Autowired
    private HttpClientService httpClientService;

    @GetMapping(value = "/file/{dir-name}/{file-name}")
    public ResponseEntity<InputStreamResource> getFiles(@PathVariable("file-name") String filename, @PathVariable("dir-name") String dirname, HttpServletRequest httpServletRequest)  {
        byte[] bytes = httpClientService.getFile(dirname, filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + filename)
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .contentLength(bytes.length)
                .body(new InputStreamResource(new ByteArrayInputStream(bytes)));
    }

    @GetMapping(value = "/userPhotos/{file-name}")
    public ResponseEntity<InputStreamResource> getUserPhotos(@PathVariable("file-name") String filename) {
        byte[] bytes = httpClientService.getUserPhoto(filename);
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(new InputStreamResource(new ByteArrayInputStream(bytes)));
    }

    @GetMapping(value = "/eventPhotos/{file-name}")
    public ResponseEntity<InputStreamResource> getEventPhotos(@PathVariable("file-name") String filename) {
        byte[] bytes = httpClientService.getEventPhoto(filename);
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(new InputStreamResource(new ByteArrayInputStream(bytes)));
    }

    @GetMapping(value = "/eventHeaders/{file-name}")
    public ResponseEntity<InputStreamResource> getEventHeader(@PathVariable("file-name") String filename) {
        byte[] bytes = httpClientService.getEventHeader(filename);
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(new InputStreamResource(new ByteArrayInputStream(bytes)));
    }






    
    //todo:maybe delete this method
    @PostMapping(value = "/uploadFiles")
    public List<String> uploadFiles(@RequestParam("files") List<MultipartFile> files){
         return httpClientService.uploadFiles(files);
        //return new ResponseEntity<>(null, HttpStatus.OK);
    }

    //todo: delete this methods
    @PostMapping(value = "/uploadPhotosUser")
    public String uploadPhotosUser(@RequestParam("file") MultipartFile file){
        return httpClientService.uploadUserPhoto(file);
    }
    @PostMapping(value = "/uploadPhotosEvent")
    public String uploadPhotosEvent(@RequestParam("file") MultipartFile file){
        return httpClientService.uploadEventPhoto(file);
    }
    @PostMapping(value = "/uploadPhotosHeader")
    public String uploadPhotosHeader(@RequestParam("file") MultipartFile file){
        return httpClientService.uploadEventHeader(file);
    }




}
