package com.onlinecourses.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.onlinecourses.forms.EventForm;
import com.onlinecourses.forms.ExtendedEventForm;
import com.onlinecourses.models.Event;
import com.onlinecourses.models.View;
import com.onlinecourses.security.details.UserDetailsImpl;
import com.onlinecourses.services.EventsService;
import com.onlinecourses.transfer.EventDto;
import com.onlinecourses.transfer.ExtendedEventDto;
import com.onlinecourses.transfer.SearchEventDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EventController {

    @Autowired
    private EventsService eventsService;

    @JsonView(value = View.SearchEvent.class)
    @GetMapping("/events")
    public List<EventDto> getEvents(){
        return eventsService.findAll();
    }

    @JsonView(value = View.ExtendedEvent.class)
    @GetMapping("/event/{id}")
    public ResponseEntity<ExtendedEventDto> findEventById(@PathVariable("id") Long eventId, @AuthenticationPrincipal UserDetailsImpl userDetails){
        return ResponseEntity.of(eventsService.findOneByUser(eventId, userDetails == null ? null : userDetails.getUser()));
    }

    @GetMapping(value = "/event/{id}/subscription", params = "action")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    public ResponseEntity<Object> subscribeUser(@PathVariable("id") Long eventId,
                                                @RequestParam(value = "action") String action,
                                                @AuthenticationPrincipal UserDetailsImpl userDetails) {
        eventsService.checkSubscriptionByUser(eventId, userDetails.getUser(), action);
        return ResponseEntity.ok().build();
    }

    @JsonView(value = View.Event.class)
    @GetMapping(value = "/events", params = "name")
    public  List<SearchEventDto> findEventsByName(@RequestParam(value = "name") String name){
        return eventsService.findAllByName(name);
    }

    @JsonView(value = {View.SearchEvent.class})
    @GetMapping(value = "/event")
    public List<SearchEventDto> findEventsByFilters(
            @RequestParam(value = "name", required = false) String name, @RequestParam(value = "tags", required = false) String tags){
        return eventsService.findAllByFilters(name, tags);
    }

    @JsonView(value = View.ExtendedEvent.class)
    @PostMapping(value = "/event")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    public ResponseEntity<ExtendedEventDto> createEvent(@AuthenticationPrincipal UserDetailsImpl userDetails,
                                             @RequestBody EventForm eventForm) {
        return ResponseEntity.of(eventsService.save(eventForm, userDetails.getUser()));
    }

    @JsonView(value = View.ExtendedEvent.class)
    @PutMapping(value = "/event/{id}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    public ResponseEntity<ExtendedEventDto> updateEvent(@RequestBody ExtendedEventForm event,
                                                        @PathVariable(name = "id") Long eventId,
                                                        @AuthenticationPrincipal UserDetailsImpl userDetails) {
        return ResponseEntity.of(eventsService.update(event, userDetails.getUser(), eventId));
    }

    @DeleteMapping("/event/{id}")
    @PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
    public ResponseEntity<Object> deleteEventById(@PathVariable("id") Long eventId, @AuthenticationPrincipal UserDetailsImpl userDetails){
        eventsService.delete(eventId, userDetails.getUser());
        return ResponseEntity.ok().build();
    }
}