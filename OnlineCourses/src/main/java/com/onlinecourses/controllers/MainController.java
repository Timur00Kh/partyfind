package com.onlinecourses.controllers;

import com.onlinecourses.utils.PropPathUtil;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @Autowired
    private PropPathUtil propPathUtil;

    @SneakyThrows
    @GetMapping({"/", "/course*", "/course/{id}", "/map", "/catalog"})
    public String getMain(ModelMap model) {
        model.addAttribute("vueDev", propPathUtil.getProperty("vue.dev"));
        model.addAttribute("vueWebpackPort", propPathUtil.getProperty("vue.webpack.port"));
        model.addAttribute("vuePojVersion", propPathUtil.getProperty("vue.pojVersion"));
        return "main";
    }
}