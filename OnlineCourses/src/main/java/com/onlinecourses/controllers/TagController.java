package com.onlinecourses.controllers;

import com.onlinecourses.services.TagService;
import com.onlinecourses.transfer.TagDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TagController {
    @Autowired
    private TagService tagService;

    @GetMapping("/tag")
    public List<TagDto> getTags() {
        return  tagService.findAll();
    }
}
