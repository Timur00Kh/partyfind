<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <title>OnlineCourses</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
    <script src="https://api-maps.yandex.ru/2.1/?apikey=20ae7440-1d9f-418d-b49b-c71d18924ffd&lang=ru_RU"
            type="text/javascript">
    </script>
</head>
<body>
<div id="app"></div>
<#if vueDev == "true">
    <script src="http://localhost:${vueWebpackPort}/main.js"></script>
<#else>
    <script src="http://localhost/js/main.js?v=${vuePojVersion}"></script>
</#if>
</body>
</html>