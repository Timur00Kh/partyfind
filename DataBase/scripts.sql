with recursive groupByEvents(event, tags) as
    (
      select event_id as event, cast(array[] as bigint[]) as tags 
        from event_tag
        union 
        select event_id, 
               groupByEvents.tags || tag_id as tags 
        from event_tag 
              join groupByEvents on 
                event_id = event and array_position(tags, tag_id) isnull 
               and event_id in (select * from sortEvents) 
    ), 
               getTags as ( 
    select distinct on (event) event, tags from groupByEvents 
    where cardinality(tags) = (select count(*) from event_tag where event_id = event) 
    ), 
               findEvents as 
   ( 
    select * from cs_event 
    where 
        id in ( 
              select event from getTags 
              where cast(tags as bigint[]) @> cast(string_to_array(:tagsString, :separator, :replacer) as bigint[]) 
              ) 
    ), 
    sortEvents as ( 
     select id from cs_event where lower(coalesce(name, ' ')) like lower('%' || :eventName || '%') 
               and (select count(*) from event_tag where id = event_id) >= cardinality(string_to_array(:tagsString, :separator, :replacer)) 
    )
select * from findEvents
----------------------------------------------------------------------------------------------					
with recursive groupByLecturers(lecturer, tags) as 
    ( 
    select lecturer_id as lecturer, cast(array[] as bigint[]) as tags 
    from lecturer_tag 
        union 
        select lecturer_id, 
               groupByLecturers.tags || tag_id as tags 
        from lecturer_tag 
               join groupByLecturers on 
                lecturer_id = event and array_position(tags, tag_id) isnull 
               and lecturer_id in (select * from sortLecturers) 
    ), 
               getTags as ( 
    select distinct on (lecturer) lecturer, tags from groupByLecturers 
    where cardinality(tags) = (select count(*) from lecturer_tag where lecturer_id = lecturer) 
    ), 
               findLecturers as 
    ( 
    select * from lecturer 
    where 
        id in ( 
              select lecturer from getTags 
              where cast(tags as bigint[]) @> cast(string_to_array(:tagsString, :separator, :replacer) as bigint[]) 
              ) 
    ), 
    sortLecturers as (
     select id from lecturer where lower(coalesce(first_name, ' ') || ' ' || coalesce(last_name, ' ')) like lower('%' || :lecturerName || '%') 
               and (select count(*) from lecturer_tag where id = lecturer_id) >= cardinality(string_to_array(:tagsString, :separator, :replacer)) 
    )
select * from findLecturers
------------------------------------------------

select * from cs_user where id in
    (select user_id from  user_event where
        user_id in (select user_id from token where value = ?1)
    and event_id in (select id from cs_event where id = ?2));
------------------------------------------------