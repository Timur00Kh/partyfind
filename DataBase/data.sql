create table if not exists cs_user (
  id bigint,
  first_name text,
  last_name text,
  email text,
  phone text,
  city text,
  avatar text,
  login text constraint login_nn not null,
  hash_password text constraint password_nn not null
);

ALTER TABLE cs_user ADD CONSTRAINT cs_user_pk PRIMARY KEY (id);
CREATE SEQUENCE online_courses_cs_user
  INCREMENT BY 1
  OWNED BY cs_user.id;
ALTER TABLE cs_user ALTER COLUMN id SET DEFAULT nextval('online_courses_cs_user');
-------------------------------
create table if not exists cs_event (
  id bigint,
  name text,
  lessons_count int constraint count_ch check (lessons_count > 0),
  description text,
  address text,
  email text,
  phone text,
  creator_id bigint,
  begin_time timestamp,
  end_time timestamp,
  x float,
  y float,
  avatar text,
  poster text
);

alter table cs_event add constraint cs_event_pk primary key (id);
create sequence online_courses_cs_event
  increment by 1
  owned by cs_event.id;
alter table cs_event alter column id set default nextval('online_courses_cs_event');
alter table cs_event add constraint cs_event_fk
foreign key (creator_id) references lecturer(id) on delete set null on update cascade;
---------------------------
create table if not exists lesson (
  id bigint,
  name text,
  description text,
  type text constraint type_nn not null,
  event_id bigint,
  index int,
  date timestamp,
  avatar text
);

alter table lesson add constraint lesson_pk primary key (id);
create sequence online_courses_lesson
  increment by 1
  owned by lesson.id;
alter table lesson alter column id set default nextval('online_courses_lesson');
alter table lesson add constraint  lesson_fk
foreign key (event_id) references cs_event(id) on delete cascade on update cascade;
----------------------------------------
create table if not exists user_event (
  user_id bigint,
  event_id bigint
);

alter table user_event add constraint  user_event_fk_event
foreign key (event_id) references cs_event(id) on delete cascade on update cascade;
alter table user_event add constraint  user_event_fk_user
foreign key (user_id) references cs_user(id) on delete cascade on update cascade;
---------------------------------------
CREATE TABLE lecturer
(
  id bigint,
  first_name text,
  last_name text,
  email text,
  phone text,
  description text,
  user_id bigint,
  avatar text
);

alter table lecturer add constraint lecturer_pk primary key (id);
create sequence online_courses_lecturer
  increment by 1
  owned by lecturer.id;
alter table lecturer alter column id set default nextval('online_courses_lecturer');
alter table lecturer add constraint lecturer_fk
foreign key (user_id) references cs_user(id) on delete set null on update cascade;
------------------------------------------------------
CREATE TABLE lecturer_event
(
  lecturer_id bigint,
  event_id bigint
);

alter table lecturer_event add constraint  lecturer_event_fk_event
foreign key (event_id) references cs_event(id) on delete cascade on update cascade;
alter table lecturer_event add constraint  lecturer_event_fk_lecturer
foreign key (lecturer_id) references lecturer(id) on delete cascade on update cascade;
---------------------------------------------
create table tag (
  id bigint,
  name text
);

alter table tag add constraint tag_pk primary key (id);
create sequence online_courses_tag
  increment by 1
  owned by tag.id;
alter table tag alter column id set default nextval('online_courses_tag');
------------------------
create table file (
  id bigint,
  name text,
  url text
);
alter table file add constraint file_pk primary key (id);
create sequence online_courses_file
  increment by 1
  owned by file.id;
alter table file alter column id set default nextval('online_courses_file');
------------------------
create table token (
  id bigint,
  value text,
  user_id bigint
);

alter table token add constraint token_pk primary key (id);
create sequence online_courses_token
  increment by 1
  owned by token.id;
alter table token alter column id set default nextval('online_courses_token');

alter table token add constraint token_user_fk
foreign key (user_id) references cs_user(id) on delete cascade on update cascade;
----------------------
create table file_lesson (
  file_id bigint,
  lesson_id bigint
);

alter table file_lesson add constraint  file_lesson_fk_file
  foreign key (file_id) references file(id) on delete cascade on update cascade;
alter table file_lesson add constraint  file_lesson_fk_lesson
  foreign key (lesson_id) references lesson(id) on delete cascade on update cascade;
----------------------------------
create table event_tag (
  event_id bigint,
  tag_id bigint
);

alter table event_tag add constraint event_tag_fk_event
foreign key (event_id) references cs_event(id) on delete cascade on update cascade;
alter table event_tag add constraint  event_tag_fk_tag
foreign key (tag_id) references tag(id) on delete cascade on update cascade;
------------------------------------
create table lecturer_tag (
  lecturer_id bigint,
  tag_id bigint
);

alter table lecturer_tag add constraint lecturer_tag_fk_lecturer
foreign key (lecturer_id) references lecturer(id) on delete cascade on update cascade;
alter table lecturer_tag add constraint lecturer_tag_fk_tag
foreign key (tag_id) references tag(id) on delete cascade on update cascade;
--------------------------