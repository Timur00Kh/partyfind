import Vue from 'vue'
import VueResource from 'vue-resource'
import App from 'pages/App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import router from 'router/rourer'
import axios from 'axios'
import 'babel-polyfill'

// register globally
import YmapPlugin from 'vue-yandex-maps'
const options = { // you may define your apiKey, lang and version or skip this.
    apiKey: '20ae7440-1d9f-418d-b49b-c71d18924ffd', // '' by default
    lang: 'ru_RU', // 'ru_RU' by default
    version: '2.1' // '2.1' by default
};
Vue.use(YmapPlugin, options);

// or for a single instance
import { yandexMap, ymapMarker } from 'vue-yandex-maps'


Vue.use(Vuetify);
Vue.use(VueResource);

router.beforeEach((to, from, next) => {
    document.title = to.meta.title;
    next()
});


new Vue({
    el: '#app',
    router,
    components: { yandexMap, ymapMarker },
    render: a => a(App)
});

let eventApi = Vue.resource("/events");